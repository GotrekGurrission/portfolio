#include "Transform.h"

Engine::Component::Transform::Transform() : Component()
{
	TranslationMatrix = DirectX::SimpleMath::Matrix::Identity;
	RotationMatrix = DirectX::SimpleMath::Matrix::Identity;
	ScaleMatrix = DirectX::SimpleMath::Matrix::Identity;
}

Engine::Component::Transform::~Transform()
{
	//TODO: Clear Transform
}

void Engine::Component::Transform::Update(const Time::FrameTime& _time, GameObject& _gameObject)
{
	TranslationMatrix = DirectX::SimpleMath::Matrix::CreateTranslation(Position);

	RotationMatrix = DirectX::SimpleMath::Matrix::CreateFromYawPitchRoll((Eulerangle * Math_t::degreeRadians));


	localMatrix = DirectX::SimpleMath::Matrix::CreateFromYawPitchRoll(DirectX::SimpleMath::Vector3(Eulerangle.x, Eulerangle.y, 0) * Math_t::degreeRadians);
	LocalForward = Position.Transform(DirectX::SimpleMath::Vector3::Forward, localMatrix);
	LocalBackward = Position.Transform(DirectX::SimpleMath::Vector3::Backward, localMatrix);
	LocalRight = Position.Transform(DirectX::SimpleMath::Vector3::Right, localMatrix);
	LocalLeft = Position.Transform(DirectX::SimpleMath::Vector3::Left, localMatrix);


	ScaleMatrix = DirectX::SimpleMath::Matrix::CreateScale(Scale);

	WorldMatrix = ScaleMatrix * RotationMatrix * TranslationMatrix;
}