#pragma once
#include <SimpleMath.h>
#include "Component.h"

namespace Engine 
{
    namespace Component 
    {
        class Transform : public ::Engine::Component::Component
        {
        public:
            Transform();
            ~Transform();
        
            void Update(const Time::FrameTime& _time, GameObject& _gameObject) override;

            virtual ComponentType Type() const override { return ComponentType::Transform; };
#pragma region Global
            /// <summary>
            /// Global Position
            /// </summary>
            DirectX::SimpleMath::Vector3 Position;
            /// <summary>
            /// Rotation als Eulerangle
            /// </summary>
            DirectX::SimpleMath::Vector3 Eulerangle;
            /// <summary>
            /// Rotation als Quaternion
            /// </summary>
            DirectX::SimpleMath::Quaternion Rotation;
            /// <summary>
            /// Skalierung
            /// </summary>
            DirectX::SimpleMath::Vector3 Scale = {1,1,1};

            /// <summary>
            /// Globale Richtung Vorw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 Forward;
            /// <summary>
            /// Globale Richtung R�ckw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 Backward;
            /// <summary>
            /// Globale Richtung Rechts
            /// </summary>
            DirectX::SimpleMath::Vector3 Right;
            /// <summary>
            /// Globale Richtung Links
            /// </summary>
            DirectX::SimpleMath::Vector3 Left;
            /// <summary>
            /// Globale Richtung Aufw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 Up;
            /// <summary>
            /// Globale Richtung Abw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 Down;
#pragma endregion

#pragma region Local
            /// <summary>
            /// Local Position
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalPosition;
            /// <summary>
            /// Local Rotation als Eulerangle
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalEulerangle;
            /// <summary>
            /// Local Rotation als Quaternion
            /// </summary>
            DirectX::SimpleMath::Quaternion LocalRotation;
            /// <summary>
            /// Skalierung im Localspace
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalScale;

            /// <summary>
            /// Lokale Richtung Vorw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalForward;
            /// <summary>
            /// Lokale Richtung R�ckw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalBackward;
            /// <summary>
            /// Lokale Richtung Rechts
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalRight;
            /// <summary>
            /// Lokale Richtung Links
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalLeft;
            /// <summary>
            /// Lokale Richtung Aufw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalUp;
            /// <summary>
            /// Lokale Richtung Abw�rts
            /// </summary>
            DirectX::SimpleMath::Vector3 LocalDown;
#pragma endregion
            DirectX::SimpleMath::Matrix localMatrix;


            DirectX::SimpleMath::Matrix WorldMatrix;

            DirectX::SimpleMath::Matrix TranslationMatrix;
            DirectX::SimpleMath::Matrix RotationMatrix;
            DirectX::SimpleMath::Matrix ScaleMatrix;
        };
    }
}