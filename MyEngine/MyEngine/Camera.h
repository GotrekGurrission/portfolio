#pragma once
#include "SimpleMath.h"
#include "Component.h"

namespace Engine
{
	namespace Component 
	{
		class Camera : public Component
		{
		public:
			Camera(int _width = 1280, int _height = 800);
			~Camera();

			void Update(const Time::FrameTime& _time, GameObject& _gameObject) override;

			DirectX::SimpleMath::Matrix ViewMatrix;
			DirectX::SimpleMath::Matrix ProjectionMatrix;

			virtual ComponentType Type() const override { return ComponentType::Camera; };
		private:
			float fov;
			float nearPlane = 1;
			float farPlane = 100;
			int width;
			int height;
			DirectX::SimpleMath::Vector3 camTarget;
		};
	}
}