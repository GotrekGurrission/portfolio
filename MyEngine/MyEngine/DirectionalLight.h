#pragma once
#include <SimpleMath.h>
#include "Component.h"

namespace Engine 
{
	namespace Component 
	{
		namespace Light 
		{
			class DirectionalLight : public Component
			{
			public:
				DirectionalLight(){}
				~DirectionalLight(){}

				void Update(const Time::FrameTime& _time, GameObject& _gameobject) override {}

				DirectX::SimpleMath::Color Color;
				DirectX::SimpleMath::Vector3 Direction;

				virtual ComponentType Type() const override { return ComponentType::DirectionalLight; };
			};
		}
	}
}