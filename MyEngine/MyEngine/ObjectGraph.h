#pragma once
#include <vector>
#include <unordered_map>
#include "EngineConfig.h"
#include "ResourceHelper.h"

namespace Engine 
{
	namespace Resource 
	{
		struct ObjectNode
		{
			resource_id Parent;
			resource_id ID;
			std::uint32_t ChildCount;
			resource_id Brother;
			resource_id FirstChild;

			ObjectNode();
			ObjectNode(resource_id _id, resource_id _parent);
			explicit ObjectNode(resource_id _id);
		};

		class ObjectGraph
		{
		public:
			ObjectGraph();
			~ObjectGraph();

			resource_id root;
			std::unordered_map<resource_id, ObjectNode> nodes;


			resource_id CreateNode(resource_id _parent = 0);

		private:
			ResourceID_Generator idGenerator;
			resource_id CreateRootNode();

			void Clear();
			void AttachChild(resource_id _parent, resource_id _child);
		};
	}
}