#pragma once
#include <vector>
#include <SimpleMath.h>
#include "EngineConfig.h"

using namespace DirectX::SimpleMath;

namespace Engine 
{
	namespace Renderer 
	{
		namespace Helper 
		{
#pragma region BufferContainer
			namespace BufferContainer 
			{
				struct BC_Mesh {
					ID3D11Buffer* vertexBuffer;
					ID3D11Buffer* indexBuffer;
					UINT vertexStride;
					UINT indexCounter;
				};

				struct BC_Shader {
					ID3D11VertexShader* vertexShader;
					ID3DBlob* vertexBlob;
					ID3D11PixelShader* pixelShader;
					ID3DBlob* pixelBlob;

					ID3D11InputLayout* inputLayout;
				};

				struct BC_Material {

				};
			}
#pragma endregion

#pragma region Buffer Structs
			namespace Buffer 
			{
				template<size_t N>
				using PadType = unsigned char[N];

				struct dxb_Camera {
					Matrix view;
					Matrix projection;
					Vector3 cameraPosition;
					PadType<1> pad;
				};

				struct dxb_Transform {
					Matrix worldMatrix;
				};

				struct dxb_DirectionalLight {
					Color color;
					Vector3 direction;
					PadType<1> pad1;
				};

				struct dxb_PointLight {
					Color color;
					Vector3 position;
					float range;
				};

				struct dxb_Material {
					//TODO: Hier kommen Texture IDS rein
					std::array<Vector4, Resource::maxParamCount> Params;
				};

				struct dxb_Object {
					int TransformID;
					int MaterialID;
					int DirecLightSize;
					int PointLightSize;
				};
			}	
#pragma endregion

#pragma region IDIndexLinker
			struct IDIndexLinker 
			{
				std::vector<Resource::resource_id> IDs;
				std::unordered_map<Resource::resource_id, int> IDToIndex;

				bool Contains(Resource::resource_id _id) const {
					return IDToIndex.find(_id) != IDToIndex.end();
				}

				int Add(Resource::resource_id _id) {
					if (!Engine::Contains(IDToIndex, _id)) {
						IDToIndex.insert_or_assign(_id, (int)IDs.size());
						IDs.push_back(_id);
					}
					return GetIndex(_id);
				}
				int GetIndex(Resource::resource_id _id) const {
					return IDToIndex.at(_id);
				}
				/// <summary>
				/// Begin from IDs
				/// </summary>
				/// <returns></returns>
				auto begin() const {
					return IDs.begin();
				}
				/// <summary>
				/// End from IDs
				/// </summary>
				/// <returns></returns>
				auto end() const {
					return IDs.end();
				}
				/// <summary>
				/// Size of IDs
				/// </summary>
				/// <returns></returns>
				auto size() const {
					return IDs.size();
				}

				void clear() {
					IDs.clear();
					IDToIndex.clear();
				}
			};
#pragma endregion
		}
	}
}