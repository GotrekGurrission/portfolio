#pragma once
#include <vector>
#include <SimpleMath.h>
#include <d3dcompiler.h>
#include "WICTextureLoader.h"
#include <d3d11_4.h>
#include <iterator>
#include "RenderCoreData.h"
#include "ResourceManager.h"
#include "Shader.h"
#include "RenderPassHelper.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "ErrorHandler.h"

#pragma comment(lib, "d3dcompiler.lib")

namespace Math = DirectX::SimpleMath;
namespace Helper = Engine::Renderer::Helper;
namespace Buffer = Engine::Renderer::Helper::Buffer;
namespace BufferContainer = Engine::Renderer::Helper::BufferContainer;
namespace Engine
{
	namespace Data
	{
		struct RenderPassData
		{
			Engine::Resource::ResourceManager* ressourceManager; 
		};
	}
	namespace Component {
		class Camera;
	}
	namespace Renderer
	{
		class RenderPass
		{
		public:
			RenderPass(const Data::RenderCoreData& _renderCoreData) : renderCoreData(_renderCoreData){}
			virtual void BuildRenderpass(const Data::RenderPassData& _data);
			virtual void UpdateData(const Data::RenderPassData& _data);
			virtual void Render(const Data::RenderPassData& _data);

		private:

		protected:
			Data::RenderCoreData renderCoreData;


			Component::Camera* mainCamera;

			std::vector<Math::Matrix> worldMatrices;
			std::vector<Component::Light::DirectionalLight*> directionalLights;
			std::vector<Component::Light::PointLight*> pointLights;

			std::vector<BufferContainer::BC_Mesh> meshBuffers;

			BufferContainer::BC_Mesh CreateMeshBuffers(const Data::MeshData* _meshData);
			void CreateMaterialBuffers(const Helper::IDIndexLinker& _materials, const Data::RenderPassData& _data);
			void CreateTransformBuffer(const std::vector<Math::Matrix>& _transforms);
			void CreateSamplerState();
			void CreateTextures(const Helper::IDIndexLinker& _textures, const Data::RenderPassData& _data);
			void CreateDirecLightBuffer();
			void CreatePointLightBuffer();

			std::vector<BufferContainer::BC_Shader> shaderBuffers;
			BufferContainer::BC_Shader CreateShaderBuffers(const Shader::Shader* _shaderData);

			void CreateConstantBuffer(UINT _size, ID3D11Buffer** _buffer);
			void CreateStructuredBuffer(UINT _elementSize, UINT _count, ID3D11Buffer** _buffer);
			template<typename T>
			void WriteBuffer(ID3D11Buffer* _buffer, T& _bufferStruct, UINT _offset = 0); 
			template<typename iterator>
				void WriteBuffer(ID3D11Buffer* _buffer, iterator _begin, iterator _end, UINT _offset = 0);
			template<typename iterator, typename converter>
				void WriteBuffer_t(ID3D11Buffer* _buffer, iterator _begin, iterator _end, converter _conv, UINT _offset = 0);

			struct RenderEntry
			{
				Resource::resource_id gameObjectID;
				Resource::resource_id worldMatrixID;
				Resource::resource_id MeshID;
				Resource::resource_id MaterialID;
				Resource::resource_id ShaderID;

			};
			std::vector<RenderEntry> renderEntries;

			ID3D11InputLayout* inputLayout;
			ID3D11SamplerState* baseSamplerState;

			ID3D11Buffer* struct_TransformBuffer;
			ID3D11ShaderResourceView* transformSRV;

			ID3D11Buffer* struct_MaterialBuffer;
			ID3D11ShaderResourceView* materialSRV;

			ID3D11Buffer* struct_DirecLightBuffer;
			ID3D11ShaderResourceView* direcLightSRV;

			ID3D11Buffer* struct_PointLightBuffer;
			ID3D11ShaderResourceView* pointLightSRV;

			ID3D11Buffer* CameraBuffer;

			ID3D11Buffer* ObjectBuffer;

			ID3D11Resource* texture = nullptr;
			std::array<ID3D11ShaderResourceView*, Resource::maxAllTexturesCount>  textureSRV;

			Helper::IDIndexLinker meshLinks;
			Helper::IDIndexLinker materialLinks;
			Helper::IDIndexLinker shaderLinks;
			Helper::IDIndexLinker textureLinks;

			std::vector<std::array<ID3D11ShaderResourceView*, Resource::maxMatTexturesCount>> materialTextureSet;
		};

#pragma region WriteBuffer Definitions
		template<typename T>
		inline void RenderPass::WriteBuffer(ID3D11Buffer* _buffer, T& _bufferStruct, UINT _offset)
		{
			D3D11_MAPPED_SUBRESOURCE Mdata = {};
			dx_except(renderCoreData.DeviceContext->Map(_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &Mdata));

			T* dataAdress = static_cast<T*>(Mdata.pData);
			dataAdress += _offset;
			std::memcpy(dataAdress, &_bufferStruct, sizeof(T));

			renderCoreData.DeviceContext->Unmap(_buffer, 0);
		}
		template<typename iterator>
		inline void RenderPass::WriteBuffer(ID3D11Buffer* _buffer, iterator _begin, iterator _end, UINT _offset)
		{
			using T = typename std::iterator_traits<iterator>::value_type;
			D3D11_MAPPED_SUBRESOURCE Mdata = {};
			dx_except(renderCoreData.DeviceContext->Map(_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &Mdata));

			T* dataAdress = static_cast<T*>(Mdata.pData);
			dataAdress += _offset;
			for (auto iter = _begin; iter != _end; ++iter)
			{
				std::memcpy(dataAdress, &*iter, sizeof(T));
				dataAdress++;
			}

			renderCoreData.DeviceContext->Unmap(_buffer, 0);
		}
		template<typename iterator, typename converter>
		inline void RenderPass::WriteBuffer_t(ID3D11Buffer* _buffer, iterator _begin, iterator _end, converter _conv, UINT _offset)
		{
			using T = typename std::iterator_traits<iterator>::value_type;
			using targetType = decltype(std::declval<converter>()(std::declval<const T&>()));
			D3D11_MAPPED_SUBRESOURCE Mdata = {};
			dx_except(renderCoreData.DeviceContext->Map(_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &Mdata));

			targetType* dataAdress = static_cast<targetType*>(Mdata.pData);
			dataAdress += _offset;
			for (auto iter = _begin; iter != _end; ++iter)
			{
				const auto& it = _conv(*iter);
				std::memcpy(dataAdress, &it, sizeof(targetType));
				dataAdress++;
			}

			renderCoreData.DeviceContext->Unmap(_buffer, 0);
		}
#pragma endregion
	}
}