#pragma once
#include "Component.h"
#include "EngineConfig.h"

namespace Engine 
{
	namespace Component 
	{
		class MeshRenderer : public Component 
		{
		public:
			MeshRenderer(Resource::resource_id _meshID, Resource::resource_id _material) : Component(), MeshID(_meshID), MaterialID(_material) {}

			Resource::resource_id MeshID;
			Resource::resource_id MaterialID;

			virtual ComponentType Type() const override { return ComponentType::MeshRenderer; };
		};
	}
}