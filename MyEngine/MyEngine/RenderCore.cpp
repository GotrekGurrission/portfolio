#include "RenderCore.h"
#include <wrl/client.h>

Engine::Renderer::RenderCore::RenderCore(Data::RenderCoreData* _renderCoreData)
{
	RenderCoreData = _renderCoreData;

	CreateDevices(RenderCoreData);
	CreateSwapChain(RenderCoreData);
	CreateDepthbuffer(RenderCoreData);
	CreateRasterizeState(RenderCoreData);
	CreateBlendState(RenderCoreData);
	CreateViewport(RenderCoreData);
}

Engine::Renderer::RenderCore::~RenderCore()
{
	//TODO: Clear RenderCore
}

void Engine::Renderer::RenderCore::BeginScene(float _rgb)
{
	BeginScene(_rgb, _rgb, _rgb);
}

void Engine::Renderer::RenderCore::BeginScene(float _r, float _g, float _b)
{
	const FLOAT color[] = { _r, _g, _b, 1.0f };

	RenderCoreData->DeviceContext->ClearRenderTargetView(renderTargetView.Get(), color);
	RenderCoreData->DeviceContext->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());
	RenderCoreData->DeviceContext->ClearDepthStencilView(depthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	RenderCoreData->DeviceContext->OMSetDepthStencilState(depthStencilState.Get(), 0);
}

void Engine::Renderer::RenderCore::EndScene()
{
	swapChain->Present(0, 0);
}

void Engine::Renderer::RenderCore::CreateDevices(Data::RenderCoreData* _renderCoreData)
{
	Microsoft::WRL::ComPtr<ID3D11Device> tempDevice;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> tempDeviceContext;

	HRESULT hr = D3D11CreateDevice(
		nullptr, //A pointer to the video adapter to use when creating a device. Pass NULL to use the default adapter, which is the first adapter enumerated by
		D3D_DRIVER_TYPE_HARDWARE, // The D3D_DRIVER_TYPE, which represents the driver type to create.
		nullptr, //A handle to a DLL that implements a software rasterizer.
		D3D11_CREATE_DEVICE_DEBUG, // The runtime layers to enable (see D3D11_CREATE_DEVICE_FLAG); values can be bitwise OR'd together.
		//D3D11_CREATE_DEVICE_DEBUGGABLE,
		Config::FeatureLevels.data(), //A pointer to an array of D3D_FEATURE_LEVELs, which determine the order of feature levels to attempt to create.
		1, //The number of elements in pFeatureLevels.
		D3D11_SDK_VERSION,
		tempDevice.GetAddressOf(),
		nullptr,
		tempDeviceContext.GetAddressOf()
	);


	tempDevice.As(&device);
	tempDeviceContext.As(&deviceContext);

	_renderCoreData->Device = device.Get();
	_renderCoreData->DeviceContext = deviceContext.Get();
}

void Engine::Renderer::RenderCore::CreateSwapChain(Data::RenderCoreData* _renderCoreData)
{
	using Microsoft::WRL::ComPtr;

	ComPtr<IDXGIDevice4> dxDevice;
	device.As(&dxDevice);

	ComPtr<IDXGIAdapter> dxgiAdapter;
	HRESULT hr4 = dxDevice->GetAdapter(&dxgiAdapter);

	ComPtr<IDXGIFactory5> dxgiFactory;
	HRESULT hr3 = dxgiAdapter->GetParent(__uuidof(IDXGIFactory5), &dxgiFactory);

	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = { 0 };
	//An integer value indicating the width of the swapchain buffers in pixels. You can use this to force your game into lower or higher resolutions than the screen shows by default. If you set it to 0, the swap chain automatically sizes itself to the current window resolution.
	swapChainDesc.Width = _renderCoreData->Width;
	//Same as the width but, you know, the height instead.
	swapChainDesc.Height = _renderCoreData->Height;
	//Scaling is a special value with two possible choices: DXGI_SCALING_STRETCH and DXGI_SCALING_NONE. Choosing the stretch option only matters if you set a custom width and height. The swap chain will stretch the image to fill the entire screen (or shrink it to fit the screen, if you chose a larger resolution). If you choose the none option, the rendered images will just appear in the top-left corner of the window.
	swapChainDesc.Scaling = DXGI_SCALING_NONE;
	//BufferCount tells the swap chain how many buffers to create. We want one front buffer and one back buffer, so we'll typically put 2 in this value.
	swapChainDesc.BufferCount = 2;
	//This value is a bit more complex. This tells the swap chain what format our pixels are stored in. In other words, what arrangement of bits are we using to store the various colors.
	swapChainDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	//This member is used to tell the swap chain how to perform anti-aliased rendering. More specifically, it tells the swap chain how many times to perform anti-aliasing on each pixel. The minimum requirement for this value is 1, which means no anti-aliasing. We'll use this because not all GPUs support it.
	swapChainDesc.SampleDesc.Count = 1;
	//This indicates the quality of the anti-aliasing. Leaving it at 0 will work for us.
	swapChainDesc.SampleDesc.Quality = 0;
	//This value is required, and it tells DXGI what this swap chain is to be used for.There's only one value we will probably ever use here, and that is DXGI_USAGE_RENDER_TARGET_OUTPUT.
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	//This is another required value that we will probably never change. It tells DXGI what to do with the buffers once they have been shown and are no longer of use. We'll use the value DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL.
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;

	HRESULT hr = dxgiFactory->CreateSwapChainForHwnd(
		_renderCoreData->Device,
		//device.Get(),
		_renderCoreData->WindowHandler,
		&swapChainDesc,
		NULL,
		NULL,
		&swapChain);

	HRESULT hr1 = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), &backBuffer);

	HRESULT hr2 = device->CreateRenderTargetView(backBuffer.Get(), nullptr, &renderTargetView);
}

void Engine::Renderer::RenderCore::CreateDepthbuffer(Data::RenderCoreData* _renderCoreData)
{
	D3D11_TEXTURE2D_DESC1 depthStencilTXTDesc;
	depthStencilTXTDesc.Width = _renderCoreData->Width;
	depthStencilTXTDesc.Height = _renderCoreData->Height;
	depthStencilTXTDesc.MipLevels = 1;
	depthStencilTXTDesc.ArraySize = 1;
	depthStencilTXTDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilTXTDesc.SampleDesc.Count = 1;
	depthStencilTXTDesc.SampleDesc.Quality = 0;
	depthStencilTXTDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilTXTDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilTXTDesc.CPUAccessFlags = 0;
	depthStencilTXTDesc.MiscFlags = 0;
	depthStencilTXTDesc.TextureLayout = D3D11_TEXTURE_LAYOUT_UNDEFINED;

	HRESULT hr = device->CreateTexture2D1(&depthStencilTXTDesc, NULL, depthStencilTexture.GetAddressOf());

	//hr = device->CreateDepthStencilView(depthStencilTexture.Get(), NULL, &depthStencilView);
	HRESULT hr1 = device->CreateDepthStencilView(depthStencilTexture.Get(), NULL, depthStencilView.GetAddressOf());

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK::D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_FUNC::D3D11_COMPARISON_LESS_EQUAL;

	HRESULT hr2 = device->CreateDepthStencilState(&depthStencilDesc, &depthStencilState);
}

void Engine::Renderer::RenderCore::CreateRasterizeState(Data::RenderCoreData* _renderCoreData)
{
	D3D11_RASTERIZER_DESC2 rsDesc = {};
	rsDesc.FrontCounterClockwise = TRUE;
	rsDesc.FillMode = D3D11_FILL_SOLID;
	rsDesc.CullMode = D3D11_CULL_BACK;

	device->CreateRasterizerState2(&rsDesc, &rasterizerState);
	//TODO: In den RenderPass
	deviceContext->RSSetState(rasterizerState.Get());
}

void Engine::Renderer::RenderCore::CreateBlendState(Data::RenderCoreData* _renderCoreData)
{
	D3D11_BLEND_DESC1 blendDesc = {};
	blendDesc.RenderTarget[0].BlendEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC1_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC1_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	HRESULT hr = device->CreateBlendState1(&blendDesc, &blendState);
	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	//TODO: In den RenderPass
	deviceContext->OMSetBlendState(blendState.Get(), blendFactor, 0xffffffff);
}

void Engine::Renderer::RenderCore::CreateViewport(Data::RenderCoreData* _renderCoreData)
{
	viewPort.Width = _renderCoreData->Width;
	viewPort.Height = _renderCoreData->Height;
	viewPort.TopLeftX = 0.0f;
	viewPort.TopLeftY = 0.0f;
	viewPort.MinDepth = 0.0f;
	viewPort.MaxDepth = 1.0f;

	deviceContext->RSSetViewports(1, &viewPort);
}
