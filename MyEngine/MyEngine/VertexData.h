#pragma once
#include <d3d11_4.h>
#include <array>
#include <SimpleMath.h>

namespace Engine 
{
	namespace Data 
	{
		static constexpr D3D11_INPUT_ELEMENT_DESC InputElement(
			LPCSTR _semanticName,
			DXGI_FORMAT _format,
			UINT _aligendOffset
		) {
			D3D11_INPUT_ELEMENT_DESC desc{};
			desc.SemanticName = _semanticName;
			desc.Format = _format;
			desc.AlignedByteOffset = _aligendOffset;
			return desc;
		}

		struct VertexData
		{
			DirectX::SimpleMath::Vector3 Position;
			DirectX::SimpleMath::Vector3 Normal;
			DirectX::SimpleMath::Vector3 Tangent;
			DirectX::SimpleMath::Vector2 UV;

			DirectX::SimpleMath::Vector4 Color;

			VertexData() :
				Position(0.0f),
				Normal(0.0f),
				Tangent(0.0f),
				UV(0.0f),
				Color(0.0f) {}

			VertexData(DirectX::SimpleMath::Vector3 _position) :
				Position(_position),
				Normal(0.0f),
				Tangent(0.0f),
				UV(0.0f),
				Color(0.0f) {}

			VertexData(DirectX::SimpleMath::Vector3 _position,
				DirectX::SimpleMath::Vector3 _normal) :
				Position(_position),
				Normal(_normal),
				Tangent(0.0f),
				UV(0.0f),
				Color(0.0f) {}

			VertexData(DirectX::SimpleMath::Vector3 _position,
				DirectX::SimpleMath::Vector3 _normal,
				DirectX::SimpleMath::Vector3 _tangent) :
				Position(_position),
				Normal(_normal),
				Tangent(_tangent),
				UV(0.0f),
				Color(0.0f) {}

			VertexData(DirectX::SimpleMath::Vector3 _position,
				DirectX::SimpleMath::Vector3 _normal,
				DirectX::SimpleMath::Vector3 _tangent,
				DirectX::SimpleMath::Vector2 _uv) :
				Position(_position),
				Normal(_normal),
				Tangent(_tangent),
				UV(_uv),
				Color(0.0f) {}

			VertexData(DirectX::SimpleMath::Vector3 _position,
				DirectX::SimpleMath::Vector3 _normal,
				DirectX::SimpleMath::Vector3 _tangent,
				DirectX::SimpleMath::Vector2 _uv,
				DirectX::SimpleMath::Vector4 _color) :
				Position(_position),
				Normal(_normal),
				Tangent(_tangent),
				UV(_uv),
				Color(_color) {}

			static constexpr auto Attributes = std::array {
							InputElement("Position", DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),
							InputElement("NORMAL", DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),
							InputElement("TANGENT", DXGI_FORMAT_R32G32B32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),
							InputElement("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT),
							InputElement("COLOR", DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_APPEND_ALIGNED_ELEMENT)
			};
		};

	}
}