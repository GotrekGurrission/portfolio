#include "GameObject.h"



Engine::GameObject::GameObject(Engine::Resource::resource_id _id) : 
	ID(_id)
{
	//TODO: Remove new
	Transform = new Engine::Component::Transform();
	components.push_back(Transform);
}

Engine::GameObject::~GameObject()
{

}

void Engine::GameObject::Update(const Time::FrameTime& _time)
{
	for (Engine::Component::Component* component : components) {
		component->Update(_time,*this);
	}
}

void Engine::GameObject::AddComponent(Engine::Component::Component* _component)
{  
	if (_component == nullptr)
		return;
	Mask |= enumv(_component->Type());
	_component->Init();
	components.push_back(_component);
}

bool Engine::GameObject::HasComponent(Engine::Component::ComponentType _type) const
{
	//constexpr auto test = enumv(Component::ComponentType::Camera);
	return enumv(_type) & Mask;
}
