#include "MeshData.h"

void Engine::Data::MeshData::GetPrimitiveCube(std::shared_ptr<MeshData> _meshData, DirectX::SimpleMath::Vector4 _color)
{
    using namespace DirectX::SimpleMath;

	_meshData->verticies.push_back({ Vector3(-0.5f, -0.5f, -0.5f),	Vector3(0, 0, -1),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, -0.5f, -0.5f),	Vector3(0, 0, -1),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f,  0.5f, -0.5f),	Vector3(0, 0, -1),	Vector3(0, 0, 0),	Vector2(0, 0), _color });
	_meshData->verticies.push_back({ Vector3(0.5f,  0.5f, -0.5f),	Vector3(0, 0, -1),	Vector3(0, 0, 0),	Vector2(1, 0), _color });

	// rechts
	_meshData->verticies.push_back({ Vector3(0.5f, -0.5f, -0.5f),	Vector3(1, 0, 0),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, -0.5f,  0.5f),	Vector3(1, 0, 0),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f,  0.5f, -0.5f),	Vector3(1, 0, 0),	Vector3(0, 0, 0),	Vector2(0, 0), _color });
	_meshData->verticies.push_back({ Vector3(0.5f,  0.5f,  0.5f),	Vector3(1, 0, 0),	Vector3(0, 0, 0),	Vector2(1, 0), _color });

	// hinten
	_meshData->verticies.push_back({ Vector3(0.5f, -0.5f,  0.5f),	Vector3(0, 0, 1),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f, -0.5f,  0.5f),	Vector3(0, 0, 1),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f,  0.5f,  0.5f),	Vector3(0, 0, 1),	Vector3(0, 0, 0),	Vector2(0, 0), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f,  0.5f,  0.5f),	Vector3(0, 0, 1),	Vector3(0, 0, 0),	Vector2(1, 0), _color });

	// links
	_meshData->verticies.push_back({ Vector3(-0.5f, -0.5f,  0.5f),	Vector3(-1, 0, 0),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f, -0.5f, -0.5f),	Vector3(-1, 0, 0),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f,  0.5f,  0.5f),	Vector3(-1, 0, 0),	Vector3(0, 0, 0),	Vector2(0, 0), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f,  0.5f, -0.5f),	Vector3(-1, 0, 0),	Vector3(0, 0, 0),	Vector2(1, 0), _color });

	// oben
	_meshData->verticies.push_back({ Vector3(-0.5f, 0.5f, -0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, 0.5f, -0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f, 0.5f,  0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(0, 0), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, 0.5f,  0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(1, 0), _color });

	// unten
	_meshData->verticies.push_back({ Vector3(-0.5f, -0.5f, -0.5f),	Vector3(0, -1, 0),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, -0.5f, -0.5f),	Vector3(0, -1, 0),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, -0.5f,  0.5f),	Vector3(0, -1, 0),	Vector3(0, 0, 0),	Vector2(0, 0), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f, -0.5f,  0.5f),	Vector3(0, -1, 0),	Vector3(0, 0, 0),	Vector2(1, 0), _color });


	for (int i = 0; i < 6; i++)
	{
		_meshData->indicies.push_back(0 + i * 4);
		_meshData->indicies.push_back(2 + i * 4);
		_meshData->indicies.push_back(3 + i * 4);

		_meshData->indicies.push_back(3 + i * 4);
		_meshData->indicies.push_back(1 + i * 4);
		_meshData->indicies.push_back(0 + i * 4);
	}

	_meshData->CalculateTangent();
}

void Engine::Data::MeshData::GetPrimitivePlane(std::shared_ptr<MeshData> _meshData, DirectX::SimpleMath::Vector4 _color)
{
	using namespace DirectX::SimpleMath;

	_meshData->verticies.push_back({ Vector3(-0.5f, 0, -0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, 0, -0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f, 0,  0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(0, 0), _color });
	_meshData->verticies.push_back({ Vector3(0.5f, 0,  0.5f),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(1, 0), _color });

	_meshData->indicies.push_back(0);
	_meshData->indicies.push_back(2);
	_meshData->indicies.push_back(3);

	_meshData->indicies.push_back(3);
	_meshData->indicies.push_back(1);
	_meshData->indicies.push_back(0);

	_meshData->CalculateTangent();
}

void Engine::Data::MeshData::GetPrimitiveTriangle(std::shared_ptr<MeshData> _meshData, DirectX::SimpleMath::Vector4 _color)
{
	using namespace DirectX::SimpleMath;

	_meshData->verticies.push_back({ Vector3( 0.5f, -0.5f, 0),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(1, 1), _color });
	_meshData->verticies.push_back({ Vector3(-0.5f, -0.5f, 0),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(0, 1), _color });
	_meshData->verticies.push_back({ Vector3(0.0f, 0.5f, 0),	Vector3(0, 1, 0),	Vector3(0, 0, 0),	Vector2(0, 0), _color });

	_meshData->indicies.push_back(0);
	_meshData->indicies.push_back(1);
	_meshData->indicies.push_back(2);

	_meshData->CalculateTangent();
}

void Engine::Data::MeshData::CalculateTangent()
{
	using namespace DirectX::SimpleMath;

	for (int x = 0; x < indicies.size(); x = x + 3) 
	{
		Vector3 edge1 = verticies[indicies[x + 1]].Position - verticies[indicies[x]].Position;
		Vector3 edge2 = verticies[indicies[x + 2]].Position - verticies[indicies[x]].Position;

		Vector2 deltaUV1 = verticies[indicies[x + 1]].UV - verticies[indicies[x]].UV;
		Vector2 deltaUV2 = verticies[indicies[x + 2]].UV - verticies[indicies[x]].UV;

		float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
		Vector3 tangente = {};
		float areaModifikation = edge1.Cross(edge2).Length() * 0.5f;
		tangente.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
		tangente.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
		tangente.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
		verticies[indicies[x]].Tangent +=		tangente * areaModifikation;
		verticies[indicies[x + 1]].Tangent +=	tangente * areaModifikation;
		verticies[indicies[x + 2]].Tangent +=	tangente * areaModifikation;
	}

	for (auto& vertex : verticies) {
		vertex.Tangent.Normalize();
	}
}
