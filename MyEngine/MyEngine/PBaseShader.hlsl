#pragma pack_matrix( row_major )
#define PI 3.14159265359

SamplerState st2d;

//Diff
//Disp
//Normal
//rough
Texture2D textures[4] : register(t3);

struct materialData
{
    float4 parameter[8];
};

struct direcLight
{
    float4 color;
    float3 direction;
    int pad;
};

struct pointLight
{
    float4 color;
    float3 position;
    float range;
};

StructuredBuffer<materialData> materialDatas : register(t0);
StructuredBuffer<direcLight> direcLights : register(t1);
StructuredBuffer<pointLight> pointLights : register(t2);

cbuffer ObjectData : register(b0)
{
    int transformID;
    int materialID;
    int direcLightSize;
    int pointLightSize;
};

cbuffer CameraData : register(b1)
{
    float4x4 view;
    float4x4 projection;
    float3 cameraPosition;
    float pad;
};

struct PS_INPUT
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
    float3x3 tbn : TANGENT;
    float2 uv : TEXCOORD;
    float4 color : COLOR;
    float4 viewPostion : VIEW;
};

float3 fresnelSchlick(float _cosTheta, float3 _F0)
{
    return _F0 + (1.0 - _F0) * pow(clamp(1.0 - _cosTheta, 0.0, 1.0), 5.0);
}

//Bi directional reflectans distribution function
// _v = viewdirection, _l lightdirection, _n = normal
float3 CalcBrdf(float3 _v, float3 _l, float3 _n, float3 _diffuseColor, float3 _specularColor, float _shininess)
{
    //Diffuse Term (lambert)
    float cosTheta = max(dot(_n, _l), 0.0f);
    float3 diffTerm = _diffuseColor;
    //FrenelTerm
    float3 frenelTerm = fresnelSchlick(cosTheta, _specularColor);
    //Speculatius
    float3 halfDir = normalize(_l + _v);
    float spec = pow(max(dot(halfDir, _n), 0.0), _shininess);
    float3 specTerm = spec * ((_shininess + 2) / (2 * PI));
    return ((diffTerm * (1 - frenelTerm)) + (frenelTerm * specTerm)) * cosTheta;
    //return specTerm;
}


float getLuminance(float3 _linearRGB)
{
    return dot(float3(0.2126729f, 0.7151522f, 0.0721750f), _linearRGB);
}

//Die _exposure_value evt hochladen �ber params
float3 ExposureMapping(float3 _lrgb, float _exposure_value)
{
    return _lrgb * pow(2, _exposure_value);
}

float3 ToneMapping(float3 _lrgb)
{
    float l_old = getLuminance(1);
    float l_new = l_old / (1.0f + l_old);
    return _lrgb * (l_new / l_old);
}

//Die 2.2 evt hochladen �ber params
float3 GammaCorrect(float3 _c)
{
    float gamma = 1.0f / 2.2f;
    return pow(_c, float3(gamma, gamma, gamma));
}

//Die 2.2 evt hochladen �ber params
float3 InverseGammaCorrect(float3 _c)
{
    return pow(_c, float3(2.2f, 2.2f, 2.2f));
}


float4 main(PS_INPUT _input) : SV_TARGET
{
    //float3 normal = normalize(_input.normal);
    float3 normal = textures[2].Sample(st2d, _input.uv).rgb * 2 - 1;
    normal = normalize(normal);
    normal = mul(normal, _input.tbn);
    
    float3 viewDir = normalize(-_input.viewPostion);
    
    //Temp Variablen
    //float3 diffColor = textures[0].Sample(st2d, _input.uv).rgb;
    float3 diffColor = InverseGammaCorrect(textures[0].Sample(st2d, _input.uv).rgb);
    float3 specColor = materialDatas[materialID].parameter[0];
    float roughness = textures[3].Sample(st2d, _input.uv).r;
    float shinnines = (2 / (roughness * roughness)) - 2;
    //float shinnines = sqrt(2 / (roughness + 2));
    float3 result = float3(0,0,0);
    //for Directional Light
    for (int dl = 0; dl < direcLightSize; dl++)
    {
        float3 f = CalcBrdf(
            viewDir,
            mul(float4(-direcLights[dl].direction, 0), view).xyz,
            normal,
            diffColor,
            specColor,
            shinnines
        );
        
        result += f * direcLights[dl].color.rgb;
    }
    //for PointLight
    for (int pl = 0; pl < pointLightSize; pl++)
    {
        float3 pointLightDir = mul(float4(pointLights[pl].position.xyz, 1), view).xyz - _input.viewPostion.xyz;
        float distance = length(pointLightDir);
        float distance2 = distance * distance;
        pointLightDir = pointLightDir / distance;
        
        float3 f = CalcBrdf(
            viewDir,
            pointLightDir,
            normal,
            diffColor,
            specColor,
            shinnines
        );
        
        result += f * (pointLights[pl].color.rgb * pointLights[pl].range) * (1 / distance2);
    }
    
    float3 ambientLight = float3(0.1f, 0.1f, 0.1f);
    //Ambient
    result += diffColor * ambientLight;
    
    //Exposure Mapping <- Mit params hochladen
    result = ExposureMapping(result, 0.0f);
    
    //ToneMapping
    result = ToneMapping(result);
    
    //Gama Korrektion
    result = GammaCorrect(result);
    //float3 ambient = ambientStrengh * ambientColor;
    return float4(result, 1);
    return float4(normal, 1);
}