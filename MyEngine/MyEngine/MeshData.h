#pragma once
#include <vector>
#include <SimpleMath.h>
#include "VertexData.h"
#include "EngineConfig.h"

namespace Engine 
{
	namespace Data 
	{
		struct MeshData 
		{
			//Engine::Resource::resource_id ID;

			std::vector<Engine::Data::VertexData> verticies = {};
			std::vector<WORD> indicies = {};

			static void GetPrimitiveCube(std::shared_ptr<MeshData> _meshData, DirectX::SimpleMath::Vector4 _color = { 1,0,0,1 });
			static void GetPrimitivePlane(std::shared_ptr<MeshData> _meshData, DirectX::SimpleMath::Vector4 _color = {1,0,0,1});
			static void GetPrimitiveTriangle(std::shared_ptr<MeshData> _meshData, DirectX::SimpleMath::Vector4 _color = {1,0,0,1});

			void CalculateTangent();
		};
	}
}

