#pragma once
#include <memory>
#include <vector>
#include <type_traits>

#include "EngineConfig.h"
#include "Component.h"
#include "Transform.h"
#include <optional>
#include <functional>

namespace Engine 
{
	class GameObject
	{
	public:
		GameObject(Engine::Resource::resource_id _id);
		~GameObject();

		void Update(const Time::FrameTime& _time);

		Engine::Component::Transform* Transform = nullptr;

		void AddComponent(Engine::Component::Component* _component);
		template <class T> T* GetComponent() const;
		template <class T> std::optional<std::reference_wrapper<T>> TryGetComponent() const;
		
		Engine::Resource::resource_id ID;
		ComponentMask Mask;

		bool HasComponent(Engine::Component::ComponentType _type) const;
	private:
		std::vector<Engine::Component::Component*> components;
		std::shared_ptr<Engine::Data::ComponentData> componentData;
	};

	template<class T>
	inline T* GameObject::GetComponent() const
	{
		static_assert(std::is_base_of_v<Engine::Component::Component, T>, "Error");

		for (Engine::Component::Component* component : components) 
		{
			auto tempComp = dynamic_cast<T*>(component);
			if (tempComp != nullptr)
				return tempComp;
		}
		return nullptr;
	}
	template<class T>
	inline std::optional<std::reference_wrapper<T>> GameObject::TryGetComponent() const
	{
		static_assert(std::is_base_of_v<Engine::Component::Component, T>, "Error");

		for (Engine::Component::Component* component : components)
		{
			auto tempComp = dynamic_cast<T*>(component);
			if (tempComp != nullptr)
				return *tempComp;
		}
		return {};
	}
}