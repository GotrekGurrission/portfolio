#include "ResourceManager.h"


Engine::Resource::ResourceManager::ResourceManager(std::shared_ptr<Engine::Data::ComponentData> _componentData) :
    ComponentData(_componentData)
{
    ClearLevel();
}

Engine::Resource::ResourceManager::~ResourceManager()
{

}

void Engine::Resource::ResourceManager::LoadLevel(Engine::Level::BaseLevel& _level)
{
    _level.Load(this);
}

void Engine::Resource::ResourceManager::ClearLevel()
{
    gameObjectGraph = Engine::Resource::ObjectGraph();

    CreateRootGameObject();
}

void Engine::Resource::ResourceManager::Update(const Time::FrameTime& _time)
{
    for(Engine::GameObject gameObject : gameObjects)
    {
        gameObject.Update(_time);
    }
}

void Engine::Resource::ResourceManager::Render()
{

}

Engine::GameObject& Engine::Resource::ResourceManager::CreateGameObject(Engine::Resource::resource_id _parent)
{
    GameObject tempGameObject = GameObject(gameObjectGraph.CreateNode(_parent));
    Engine::Resource::resource_id tempID = tempGameObject.ID;

    if (tempGameObject.ID >= gameObjects.size()) {
        gameObjects.push_back(std::move(tempGameObject));
    }
    else
    {
        gameObjects[tempGameObject.ID] = std::move(tempGameObject);
    }
    return gameObjects[tempID];
}

Engine::GameObject& Engine::Resource::ResourceManager::CreateRootGameObject()
{
    GameObject tempGameObject = GameObject(gameObjectGraph.root);

    if (tempGameObject.ID >= gameObjects.size()) {
        gameObjects.push_back(std::move(tempGameObject));
    }
    else
    {
        gameObjects[tempGameObject.ID] = std::move(tempGameObject);
    }
    return gameObjects[gameObjectGraph.root];
}

Engine::Resource::resource_id Engine::Resource::ResourceManager::CreateMesh(const std::string& _name)
{
    auto tempMesh = std::make_shared<Engine::Data::MeshData>();
    auto tempID = meshes.Add(std::move(tempMesh), _name);
    return tempID;
}

Engine::Resource::resource_id Engine::Resource::ResourceManager::CreateShader(const std::string& _name)
{
    std::string vertexFileName = "V" + _name + ".hlsl";
    std::string pixelFileName = "P" + _name + ".hlsl";

    std::wstring vertexFilePath = (Resource::ShaderDirectory / vertexFileName).wstring();
    std::wstring pixelFilePath = (Resource::ShaderDirectory / pixelFileName).wstring();

    if (std::filesystem::exists(Resource::ShaderDirectory / vertexFileName) && std::filesystem::exists(Resource::ShaderDirectory / pixelFileName)) {
        auto tempShader = std::make_unique<Shader::Shader>(vertexFilePath, pixelFilePath);
        auto tempID = shaders.Add(std::move(tempShader), _name);
        return tempID;
    }
    else
    {
        throw std::runtime_error("Shader File " + _name + " nicht gefunden");
    }
}

Engine::Resource::resource_id Engine::Resource::ResourceManager::CreateMaterial(const std::string& _name)
{
    auto tempsMaterial = std::make_unique<Engine::Data::MaterialData>();
    auto tempID = materials.Add(std::move(tempsMaterial), _name);
    return tempID;
}

Engine::Resource::resource_id Engine::Resource::ResourceManager::CreateTexture(const std::string& _name)
{
    std::wstring FilePath = (Resource::TextureDirectory / _name).wstring();
    if (std::filesystem::exists(Resource::TextureDirectory / _name))
    {
        auto tempTexture = std::make_unique<Shader::Texture>(FilePath);
        auto tempID = textures.Add(std::move(tempTexture), _name);
        return tempID;
    }
    else
    {
        throw std::runtime_error("Texture File " + _name + " nicht gefunden");
    }
}
