#include "MyEngine.h"
#include <iostream>
#include <Windows.h>
#include "Core.h"
#include "WindowData.h"


int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nShowCmd)
{
	Engine::Data::WindowData windowData;
	windowData.HInstance = hInstance;
	windowData.HPrevInstance = hPrevInstance;
	windowData.LpCmdLine = lpCmdLine;
	windowData.NShowCmd = nShowCmd;

	try {
		Engine::Core core = Engine::Core(windowData);
		core.InitGameloop();
	}
	catch (std::exception& _ex) {
		std::cout << _ex.what();
		char a;
		std::cin >> a;
	}


	return 0;
}


int main() {
	return WinMain(GetModuleHandle(NULL), NULL, GetCommandLineA(), SW_SHOWNORMAL);
}