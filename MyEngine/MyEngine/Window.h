#pragma once
#include "WindowData.h"
#include "Keyboard.h"

namespace Engine 
{
	class Window
	{
	public:
		Window(Data::WindowData _windowData);
		~Window();
	
		Data::WindowData WindowData;

		bool IsRun();
		
		std::unique_ptr<DirectX::Keyboard> keyboard;
	};
}