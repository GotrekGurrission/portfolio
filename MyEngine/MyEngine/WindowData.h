#pragma once
#include <Windows.h>

namespace Engine 
{
	namespace Data 
	{
		struct WindowData
		{
			HINSTANCE HInstance;
			HINSTANCE HPrevInstance;
			LPSTR LpCmdLine;
			int NShowCmd;
			int Width = 1280;
			int Height = 720;

			LPCWSTR Name = L"Windowname";

			HWND WindowHandler;
		};
	}
}