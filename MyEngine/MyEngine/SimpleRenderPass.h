#pragma once
#include "RenderPass.h"
#include <vector>
#include <SimpleMath.h>

namespace Engine 
{
	namespace Renderer 
	{
		class SimpleRenderPass : public RenderPass
		{
		public:
			SimpleRenderPass(const Data::RenderCoreData& _renderCoreData) : RenderPass(_renderCoreData){}
			virtual void BuildRenderpass(const Data::RenderPassData& _data) override;
			virtual void UpdateData(const Engine::Data::RenderPassData& _data) override;
			virtual void Render(const Engine::Data::RenderPassData& _data) override;
		};
	}
}