#pragma once
#include<SimpleMath.h>
#include "Component.h"

namespace Engine 
{
	namespace Component 
	{
		namespace Light 
		{
			class PointLight : public Component 
			{
			public:
				PointLight() {}
				~PointLight() {}

				void Update(const Time::FrameTime& _time, GameObject& _gameobject) override;

				DirectX::SimpleMath::Color Color;
				DirectX::SimpleMath::Vector3 Position;
				float Range;

				virtual ComponentType Type() const override { return ComponentType::PointLight; };
			};
		}
	}
}