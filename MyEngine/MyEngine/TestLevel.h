#pragma once
#include "BaseLevel.h"
#include "MeshData.h"

namespace Engine 
{
	namespace Level 
	{
		class TestLevel : public BaseLevel
		{
		public:
			virtual void Load(Engine::Resource::ResourceManager* _ressourceManager) override;
		};
	}
}