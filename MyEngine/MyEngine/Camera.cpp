#include "Camera.h"
#include "GameObject.h"

Engine::Component::Camera::Camera(int _width, int _height)
	: Component(), width(_width), height(_height)
{
	fov = DirectX::XMConvertToRadians(90);
}

Engine::Component::Camera::~Camera()
{
	//TODO: Clear Camera
}

void Engine::Component::Camera::Update(const Time::FrameTime& _time, GameObject& _gameObject)
{
	camTarget = DirectX::SimpleMath::Vector3::Transform(DirectX::SimpleMath::Vector3::Forward, _gameObject.Transform->RotationMatrix);
	camTarget += _gameObject.Transform->Position;

	ViewMatrix = _gameObject.Transform->WorldMatrix.Invert();

	//TODO: Daten aus der Config bekommen
	ProjectionMatrix = DirectX::SimpleMath::Matrix::CreatePerspectiveFieldOfView(
		fov, 
		static_cast<float>(width) / static_cast<float>(height), 
		nearPlane, 
		farPlane);
}