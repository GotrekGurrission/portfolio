#include "PointLight.h"
#include "GameObject.h"

void Engine::Component::Light::PointLight::Update(const Time::FrameTime& _time, GameObject& _gameobject)
{
	Position = _gameobject.GetComponent<Transform>()->Position;
}
