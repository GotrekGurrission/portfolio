#include "RenderPass.h"
#include <d3d11_4.h>
#include <iostream>

namespace Buffer = Engine::Renderer::Helper::Buffer;

void Engine::Renderer::RenderPass::BuildRenderpass(const Data::RenderPassData& _data)
{
}

void Engine::Renderer::RenderPass::UpdateData(const Data::RenderPassData& _data)
{
}

void Engine::Renderer::RenderPass::Render(const Data::RenderPassData& _data)
{

}

void Engine::Renderer::RenderPass::CreateConstantBuffer(UINT _size, ID3D11Buffer** _buffer) 
{
    D3D11_BUFFER_DESC desc = {};
    desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    desc.ByteWidth = _size;
    desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    dx_except(renderCoreData.Device->CreateBuffer(&desc, nullptr, _buffer));
}

void Engine::Renderer::RenderPass::CreateStructuredBuffer(UINT _elementSize, UINT _count, ID3D11Buffer** _buffer) 
{
    D3D11_BUFFER_DESC desc = {};
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    //desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
    desc.ByteWidth = _elementSize * _count;
    desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
    desc.StructureByteStride = _elementSize;

    dx_except(renderCoreData.Device->CreateBuffer(&desc, nullptr, _buffer));
}

void Engine::Renderer::RenderPass::CreateMaterialBuffers(const Helper::IDIndexLinker& _materials,   const Data::RenderPassData& _data)
{
    CreateStructuredBuffer(sizeof(Buffer::dxb_Material), _materials.size(), &struct_MaterialBuffer);

    WriteBuffer_t(struct_MaterialBuffer, _materials.begin(), _materials.end(), [&_data](const auto& params) {return Buffer::dxb_Material{ _data.ressourceManager->materials.Get(params)->Params }; });

    D3D11_SHADER_RESOURCE_VIEW_DESC desc{};
    desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
    desc.Format = DXGI_FORMAT_UNKNOWN;
    desc.Buffer.FirstElement = 0;
    desc.Buffer.NumElements = _materials.size();
    
    renderCoreData.Device->CreateShaderResourceView(struct_MaterialBuffer, &desc, &materialSRV);

    //D3D11_MAPPED_SUBRESOURCE Mdata = {};
    //dx_except(renderCoreData.DeviceContext->Map(struct_MaterialBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &Mdata));

    //Buffer::dxb_Material* dxbmP = static_cast<Buffer::dxb_Material*>(Mdata.pData);
    //for (auto materialID : _materials) {
    //    const auto& material = _data.ressourceManager->materials.Get(materialID);
    //    Buffer::dxb_Material dxbm{};

    //    dxbm.Params = material->Params;
    //    *dxbmP = dxbm;
    //    dxbmP++;
    //}
    //renderCoreData.DeviceContext->Unmap(struct_MaterialBuffer, 0);
}

void Engine::Renderer::RenderPass::CreateTransformBuffer(const std::vector<Math::Matrix>& _transforms)
{
    CreateStructuredBuffer( sizeof(Buffer::dxb_Transform), _transforms.size(), &struct_TransformBuffer);
    //CreateStructuredBuffer(_transforms.size() * sizeof(Math::Matrix), &struct_TransformBuffer);

    WriteBuffer_t(struct_TransformBuffer, _transforms.begin(), _transforms.end(), [](const Math::Matrix& m) {return Buffer::dxb_Transform{ m }; });
    //WriteBuffer_t(struct_TransformBuffer, _transforms.begin(), _transforms.end(), [](const Math::Matrix& m) {return m; });

    D3D11_SHADER_RESOURCE_VIEW_DESC desc{};
    desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
    desc.Format = DXGI_FORMAT_UNKNOWN;
    desc.Buffer.FirstElement = 0;
    desc.Buffer.ElementWidth = sizeof(Buffer::dxb_Transform);
    //desc.Buffer.ElementWidth = 64;
    desc.Buffer.NumElements = _transforms.size();
    //desc.Buffer.ElementOffset = 0;

    constexpr size_t test = sizeof(Buffer::dxb_Transform);
    std::cout << desc.Buffer.ElementWidth;
    dx_except(renderCoreData.Device->CreateShaderResourceView(struct_TransformBuffer, &desc, &transformSRV));
    //renderCoreData.Device->CreateShaderResourceView(struct_TransformBuffer, &desc, &transformSRV);

    //D3D11_MAPPED_SUBRESOURCE Mdata = {};
    //dx_except(renderCoreData.DeviceContext->Map(struct_TransformBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &Mdata));

    //Buffer::dxb_Transform* dxbtP = static_cast<Buffer::dxb_Transform*>(Mdata.pData);
    //for (auto& transform : _transforms) {
    //    Buffer::dxb_Transform dxbt{};
    //    dxbt.worldMatrix = transform;
    //    *dxbtP = dxbt;
    //    dxbtP++;
    //}
    //renderCoreData.DeviceContext->Unmap(struct_TransformBuffer, 0);
}

void Engine::Renderer::RenderPass::CreateSamplerState()
{
    D3D11_SAMPLER_DESC sampdesc;
    ZeroMemory(&sampdesc, sizeof(sampdesc));
    sampdesc.Filter = D3D11_FILTER_ANISOTROPIC;
    //sampdesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampdesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampdesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampdesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampdesc.MaxAnisotropy = 16;
    sampdesc.MinLOD = 0;
    sampdesc.MaxLOD = D3D11_FLOAT32_MAX;

    dx_except(renderCoreData.Device->CreateSamplerState(&sampdesc, &baseSamplerState));
}

void Engine::Renderer::RenderPass::CreateTextures(const Helper::IDIndexLinker& _textures, const Data::RenderPassData& _data)
{
    int index = 0;
    for (auto textureID : _textures) {
        dx_except(DirectX::CreateWICTextureFromFile(
            renderCoreData.Device, renderCoreData.DeviceContext,
            _data.ressourceManager->textures.Get(textureID)->FileName.c_str(),
            &texture, &textureSRV[index++]));
    }
}

void Engine::Renderer::RenderPass::CreateDirecLightBuffer()
{
    CreateStructuredBuffer(sizeof(Buffer::dxb_DirectionalLight), directionalLights.size(), &struct_DirecLightBuffer);

    D3D11_SHADER_RESOURCE_VIEW_DESC desc{};
    desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
    desc.Format = DXGI_FORMAT_UNKNOWN;
    desc.Buffer.FirstElement = 0;
    desc.Buffer.NumElements = directionalLights.size();

    renderCoreData.Device->CreateShaderResourceView(struct_DirecLightBuffer, &desc, &direcLightSRV);
}

void Engine::Renderer::RenderPass::CreatePointLightBuffer()
{
    CreateStructuredBuffer(sizeof(Buffer::dxb_DirectionalLight), pointLights.size(), &struct_PointLightBuffer);

    D3D11_SHADER_RESOURCE_VIEW_DESC desc{};
    desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
    desc.Format = DXGI_FORMAT_UNKNOWN;
    desc.Buffer.FirstElement = 0;
    desc.Buffer.NumElements = pointLights.size();

    renderCoreData.Device->CreateShaderResourceView(struct_PointLightBuffer, &desc, &pointLightSRV);
}

BufferContainer::BC_Shader Engine::Renderer::RenderPass::CreateShaderBuffers(const Shader::Shader* _shaderData)
{
    ID3DBlob* errorBlob{};
    BufferContainer::BC_Shader shaderBuffers{};
    HRESULT hr = D3DCompileFromFile(
        _shaderData->vName.c_str(),			//Shader Filename
        nullptr, nullptr,		    //optional macros & includes
        _shaderData->vEntryPoint.c_str(),	//Name der Einstiegsfunktion
        _shaderData->vShaderType.c_str(),	//Shadertyp und Version
        0, 0,					    //Optional Flag
        &shaderBuffers.vertexBlob,	//Compiled Blob Target
        &errorBlob					    //Optional Blob for all                                     compile errors
    );
    if (errorBlob) {
        const char* vmessage = reinterpret_cast<const char*>(errorBlob->GetBufferPointer());
        dx_except_m(hr, vmessage);
    }

    dx_except(renderCoreData.Device->CreateVertexShader(shaderBuffers.vertexBlob->GetBufferPointer(), shaderBuffers.vertexBlob->GetBufferSize(), nullptr, &shaderBuffers.vertexShader));

    D3DCompileFromFile(
        _shaderData->pName.c_str(),			//Shader Filename
        nullptr, nullptr,		    //optional macros & includes
        _shaderData->pEntryPoint.c_str(),	//Name der Einstiegsfunktion
        _shaderData->pShaderType.c_str(),	//Shadertyp und Version
        0, 0,					    //Optional Flag
        &shaderBuffers.pixelBlob,	//Compiled Blob Target
        &errorBlob					    //Optional Blob for all compile errors
    );

    if (errorBlob) {
        const char* pmessage = reinterpret_cast<const char*>(errorBlob->GetBufferPointer());
        dx_except_m(hr, pmessage);
    }

    dx_except(renderCoreData.Device->CreatePixelShader(shaderBuffers.pixelBlob->GetBufferPointer(), shaderBuffers.pixelBlob->GetBufferSize(), nullptr, &shaderBuffers.pixelShader));

    return shaderBuffers;
}

BufferContainer::BC_Mesh Engine::Renderer::RenderPass::CreateMeshBuffers(const Data::MeshData* _meshData)
{
    BufferContainer::BC_Mesh mesh;
    mesh.vertexStride = sizeof(Engine::Data::VertexData);

    D3D11_BUFFER_DESC VDesc = {};
    VDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    VDesc.ByteWidth = _meshData->verticies.size() * mesh.vertexStride;

    D3D11_SUBRESOURCE_DATA VSubData = {};
    VSubData.pSysMem = _meshData->verticies.data();

    dx_except(renderCoreData.Device->CreateBuffer(&VDesc, &VSubData, &mesh.vertexBuffer));

    D3D11_BUFFER_DESC IDesc = {};
    IDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    IDesc.ByteWidth = _meshData->indicies.size() * sizeof(WORD);

    mesh.indexCounter = _meshData->indicies.size();

    D3D11_SUBRESOURCE_DATA ISubData = {};
    ISubData.pSysMem = _meshData->indicies.data();

    dx_except(renderCoreData.Device->CreateBuffer(&IDesc, &ISubData, &mesh.indexBuffer));

    return mesh;
}
