#pragma once
#include<string>
#include <d3d11_4.h>

namespace Engine 
{
	namespace Shader 
	{
		class Shader 
		{
		public:
			Shader(){}
			Shader(const std::wstring& _vName, const std::wstring& _pName) : vName(_vName), pName(_pName){}

			std::wstring vName = L"VDefault.hlsl";
			std::string vEntryPoint = "main";
			std::string vShaderType = "vs_4_0";

			std::wstring pName = L"PDefault.hlsl";
			std::string pEntryPoint = "main";
			std::string pShaderType = "ps_4_0";
		};
	}
}