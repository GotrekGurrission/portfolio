#pragma once

namespace Engine 
{
	namespace Resource 
	{
		class ResourceManager;
	}
	namespace Level 
	{
		class BaseLevel
		{
		public:
			virtual void Load(Engine::Resource::ResourceManager* _ressourceManager) = 0;
		};
	}
}