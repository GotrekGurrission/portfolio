#pragma pack_matrix( row_major )

StructuredBuffer<float4x4> worldMatrices : register(t0);

cbuffer ObjectData : register(b0)
{
    int transformID;
    int materialID;
};

cbuffer CameraData : register(b1)
{
    float4x4 view;
    float4x4 projection;
    float3 cameraPosition;
    float pad;
};

struct VS_INPUT
{
    float4 position : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD;
    float4 color : COLOR;
};

struct VS_OUTPUT
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD;
    float4 color : COLOR;
};


VS_OUTPUT main(VS_INPUT _input)
{
    //float4x4 wvp = mul(worldPosition, mul(view, projection));
    VS_OUTPUT output;
    //_input.position.w = 1.0f;
    //output.position = mul(_input.position, wvp);
    output.position = _input.position;
    output.position.w = 1.0f;
    ////output.position = _input.position;
    //output.normal = _input.normal;
    //output.uv = _input.uv;
    //output.color = _input.color;
    return output;
}