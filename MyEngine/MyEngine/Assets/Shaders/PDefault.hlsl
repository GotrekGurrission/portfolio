
struct materialData
{
    float4 parameter[8];
};

StructuredBuffer<materialData> worldMatrices : register(t0);

cbuffer ObjectData : register(b0)
{
    int transformID;
    int materialID;
};

struct PS_INPUT
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD;
    float4 color : COLOR;
};

float4 main(PS_INPUT _input) : SV_TARGET
{
    return float4(1.0f, 0, 0, 1.0f);
    //return _input.color;
    //return float4(directLights[0].Color, 1.0f);
}