#pragma once
#include <vector>
#include "EngineConfig.h"
#include <unordered_map>

namespace Engine 
{
	namespace Scene 
	{
		struct SceneNode
		{
			scene_id Parent;
			scene_id ID;
			std::uint32_t ChildCount;
			scene_id Brother;
			scene_id FirstChild;

			SceneNode();
			explicit SceneNode(scene_id _id);
			SceneNode(scene_id _id, scene_id _parent);
		};

		class SceneGraph
		{
		public:
			SceneGraph();
			~SceneGraph();

			scene_id root;
			scene_id currentCount;
			std::unordered_map<scene_id, SceneNode> nodes;

			std::vector<scene_id> openIDs;
			scene_id CreateNode(scene_id _parent = 0);

		private:
			scene_id GetNewId();
			scene_id CreateRootNode();
			void Clear();
			void ReleaseID(scene_id _id);
			void AttachChild(scene_id _parent, scene_id _child);
		};
	}
}