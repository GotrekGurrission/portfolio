#include "SimpleRenderPass.h"
#include "Transform.h"
#include "Camera.h"
#include "MeshRenderer.h"
#include "VertexData.h"
#include <unordered_set>
#include <algorithm>

void Engine::Renderer::SimpleRenderPass::BuildRenderpass(const Data::RenderPassData& _data)
{
	meshLinks.clear();
	materialLinks.clear();
	shaderLinks.clear();
	worldMatrices.clear();
	textureLinks.clear();
	materialTextureSet.clear();

	directionalLights.clear();
	pointLights.clear();

	std::vector<std::array<Resource::resource_id, Resource::maxMatTexturesCount>> textureIDSets{};

	constexpr size_t teca = sizeof(Buffer::dxb_Camera);
	CreateConstantBuffer(sizeof(Buffer::dxb_Camera), &CameraBuffer);

	constexpr size_t test = sizeof(Buffer::dxb_Object);
	CreateConstantBuffer(test, &ObjectBuffer);

	for (const auto& object : _data.ressourceManager->AllGameObjects())
	{
		if (auto camera = object.TryGetComponent<Component::Camera>())
		{
			mainCamera = &camera->get();
		}

		if (object.HasComponent(Component::ComponentType::DirectionalLight)) 
		{
			directionalLights.push_back(object.GetComponent<Component::Light::DirectionalLight>());
		}

		if (object.HasComponent(Component::ComponentType::PointLight)) 
		{
			pointLights.push_back(object.GetComponent<Component::Light::PointLight>());
		}

		if (object.HasComponent(Component::ComponentType::MeshRenderer))
		{
			RenderEntry tempEntry{};

			tempEntry.gameObjectID = object.ID;

			worldMatrices.push_back(object.GetComponent<Component::Transform>()->WorldMatrix);
			tempEntry.worldMatrixID = worldMatrices.size() - 1;

			tempEntry.MeshID = meshLinks.Add(object.GetComponent<Component::MeshRenderer>()->MeshID);
			 
			if (materialLinks.Contains(object.GetComponent<Component::MeshRenderer>()->MaterialID)) {
				tempEntry.MaterialID = materialLinks.Add(object.GetComponent<Component::MeshRenderer>()->MaterialID);
			}
			else
			{
				auto rawMaterialId = object.GetComponent<Component::MeshRenderer>()->MaterialID;
				tempEntry.MaterialID = materialLinks.Add(rawMaterialId);

				std::array<Resource::resource_id, Resource::maxMatTexturesCount> tempSet{};
				int index = 0;
				for (auto textureID : _data.ressourceManager->materials.Get(rawMaterialId)->Textures)
				{
					tempSet[index++] = textureLinks.Add(textureID);
				}
				textureIDSets.push_back(tempSet);
			}

			tempEntry.ShaderID = shaderLinks.Add(_data.ressourceManager->materials.Get(tempEntry.MaterialID)->Shader);

			renderEntries.push_back(tempEntry);
		}
	}

	CreateSamplerState();

	CreateTextures(textureLinks, _data);
	for (int s = 0; s < textureIDSets.size(); s++) 
	{
		std::array<ID3D11ShaderResourceView*, Resource::maxMatTexturesCount> temp{};
		for (int t = 0; t < Resource::maxMatTexturesCount; t++) {
			temp[t] = textureSRV[textureIDSets.at(s)[t]];
		}
		materialTextureSet.push_back(temp);
	}

	for (const auto& meshID : meshLinks) {
		meshBuffers.push_back(CreateMeshBuffers(_data.ressourceManager->meshes.Get(meshID).get()));
	}

	for (const auto& shaderID : shaderLinks) {
		shaderBuffers.push_back(CreateShaderBuffers(_data.ressourceManager->shaders.Get(shaderID).get()));

		dx_except(renderCoreData.Device->CreateInputLayout(Data::VertexData::Attributes.data(), Data::VertexData::Attributes.size(), shaderBuffers.back().vertexBlob->GetBufferPointer(), shaderBuffers.back().vertexBlob->GetBufferSize(), &shaderBuffers.back().inputLayout));
	}

	CreateMaterialBuffers(materialLinks, _data);

	CreateTransformBuffer(worldMatrices);

	CreateDirecLightBuffer();
	CreatePointLightBuffer();

	WriteBuffer_t(
		struct_DirecLightBuffer,
		directionalLights.begin(),
		directionalLights.end(),
		[](const Component::Light::DirectionalLight* _light)
		{
			return Buffer::dxb_DirectionalLight{
				_light->Color,
				_light->Direction
			};
		});

	WriteBuffer_t(
		struct_PointLightBuffer,
		pointLights.begin(),
		pointLights.end(),
		[](const Component::Light::PointLight* _light)
		{
			return Buffer::dxb_PointLight{
				_light->Color,
				_light->Position,
				_light->Range
			};
		});
}

void Engine::Renderer::SimpleRenderPass::UpdateData(const Engine::Data::RenderPassData& _data)
{
	//Camera Buffer Updaten
	Buffer::dxb_Camera cam{};
	cam.cameraPosition = { -mainCamera->ViewMatrix._41, -mainCamera->ViewMatrix._42, -mainCamera->ViewMatrix._43 };
	cam.view = mainCamera->ViewMatrix;
	cam.projection = mainCamera->ProjectionMatrix;
	WriteBuffer(CameraBuffer, cam);

	for (const auto& entry : renderEntries) 
	{
		worldMatrices[entry.worldMatrixID] = _data.ressourceManager->AllGameObjects()[entry.gameObjectID].Transform->WorldMatrix;
	}

	WriteBuffer_t(
		struct_MaterialBuffer, 
		materialLinks.begin(), materialLinks.end(), 
		[&_data](const Resource::resource_id& id) 
		{
			return Buffer::dxb_Material{ _data.ressourceManager->materials.Get(id)->Params};
		});

	WriteBuffer_t(
		struct_TransformBuffer, 
		worldMatrices.begin(), 
		worldMatrices.end(), 
		[](const Math::Matrix& m) 
		{
			return Buffer::dxb_Transform{ m }; 
		});

	WriteBuffer_t(
		struct_DirecLightBuffer,
		directionalLights.begin(),
		directionalLights.end(),
		[](const Component::Light::DirectionalLight* _light)
		{
			return Buffer::dxb_DirectionalLight{
				_light->Color,
				_light->Direction
			};
		});

	WriteBuffer_t(
		struct_PointLightBuffer,
		pointLights.begin(),
		pointLights.end(),
		[](const Component::Light::PointLight* _light)
		{
			return Buffer::dxb_PointLight{
				_light->Color,
				_light->Position,
				_light->Range
			};
		});
}

void Engine::Renderer::SimpleRenderPass::Render(const Engine::Data::RenderPassData& _data)
{
	for (const auto& entry : renderEntries) 
	{
		renderCoreData.DeviceContext->IASetInputLayout(shaderBuffers.at(entry.ShaderID).inputLayout);
		renderCoreData.DeviceContext->VSSetShader(shaderBuffers.at(entry.ShaderID).vertexShader, nullptr, 0);
		renderCoreData.DeviceContext->PSSetShader(shaderBuffers.at(entry.ShaderID).pixelShader, nullptr, 0);

		renderCoreData.DeviceContext->VSSetConstantBuffers(1, 1, &CameraBuffer);
		renderCoreData.DeviceContext->PSSetConstantBuffers(1, 1, &CameraBuffer);

		Buffer::dxb_Object objBuffer{};
		objBuffer.TransformID = entry.worldMatrixID;
		objBuffer.MaterialID = entry.MaterialID;
		objBuffer.DirecLightSize = directionalLights.size();
		objBuffer.PointLightSize = pointLights.size();
		WriteBuffer(ObjectBuffer, objBuffer);
		renderCoreData.DeviceContext->VSSetConstantBuffers(0, 1, &ObjectBuffer);
		renderCoreData.DeviceContext->PSSetConstantBuffers(0, 1, &ObjectBuffer);

		
		renderCoreData.DeviceContext->VSSetShaderResources(0, 1, &transformSRV);
		renderCoreData.DeviceContext->PSSetShaderResources(0, 1, &materialSRV);
		renderCoreData.DeviceContext->PSSetShaderResources(1, 1, &direcLightSRV);
		renderCoreData.DeviceContext->PSSetShaderResources(2, 1, &pointLightSRV);


		renderCoreData.DeviceContext->PSSetSamplers(0, 1, &baseSamplerState);
		
		renderCoreData.DeviceContext->PSSetShaderResources(3, Resource::maxMatTexturesCount, materialTextureSet[entry.MaterialID].data());








		static UINT offset = 0;
		renderCoreData.DeviceContext->IASetVertexBuffers(0, 1, &meshBuffers.at(entry.MeshID).vertexBuffer, &meshBuffers.at(entry.MeshID).vertexStride, &offset);
		int x = 0;
		renderCoreData.DeviceContext->IASetIndexBuffer(meshBuffers.at(entry.MeshID).indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		renderCoreData.DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		renderCoreData.DeviceContext->DrawIndexed(meshBuffers.at(entry.MeshID).indexCounter, 0, 0);
	}
	//ObjectBuffer Updaten
	//Shader Stuff ?
	//Mesh Malen
}
