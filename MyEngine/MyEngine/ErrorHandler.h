#pragma once
#include <stdexcept>
#include <d3d11_4.h>
#include <sstream>
#include <string_view>

namespace Engine 
{
	namespace ErrorHandler
	{
		inline constexpr std::string_view hresult_to_string(HRESULT _hr) noexcept
		{
			switch (_hr)
			{
			case D3D11_ERROR_FILE_NOT_FOUND:
				return "D3D11_ERROR_FILE_NOT_FOUND (0x887C0002) The file was not found.";
				break;
			case D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS:
				return "D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS (0x887C0001) There are too many unique instances of a particular type of state object.";
				break;
			case D3D11_ERROR_TOO_MANY_UNIQUE_VIEW_OBJECTS:
				return "D3D11_ERROR_TOO_MANY_UNIQUE_VIEW_OBJECTS (0x887C0003) There are too many unique instances of a particular type of view object.";
				break;
			case D3D11_ERROR_DEFERRED_CONTEXT_MAP_WITHOUT_INITIAL_DISCARD:
				return "D3D11_ERROR_DEFERRED_CONTEXT_MAP_WITHOUT_INITIAL_DISCARD (0x887C0004)The first call to ID3D11DeviceContext::Map after either ID3D11Device::CreateDeferredContext or ID3D11DeviceContext::FinishCommandList per Resource was not D3D11_MAP_WRITE_DISCARD.";
				break;
			case E_FAIL:
				return "E_FAIL: Nicht spezifizierter Fehler";
				break;
			case E_INVALIDARG:
				return "E_INVALIDARG: Mindestens ein Argument ist ung�ltig.";
				break;
			case E_OUTOFMEMORY:
				return "E_OUTOFMEMORY: Fehler beim Zuweisen des erforderlichen Arbeitsspeichers";
				break;
			case E_NOTIMPL:
				return "E_NOTIMPL: Nicht implementiert";
				break;
			default:
				return "Fehlerwert noch nicht erfasst!";
				break;
			}
		}

		inline HRESULT dx_Error_Message(HRESULT _ret, const char* _file, int _line, const char* _call)
		{
			if (_ret == S_OK || _ret == S_FALSE)
				return _ret;
			else
			{
				std::stringstream sstr;
				sstr << "Micosoft call failed: (" << _call << ")" << std::endl <<
					"\tfile: " << _file << std::endl <<
					"\tline: " << _line << std::endl <<
					"\treturn code:" << Engine::ErrorHandler::hresult_to_string(_ret) << std::endl;
				throw std::runtime_error(sstr.str().c_str());
			}
		}

		inline HRESULT dx_Error_Message(HRESULT _ret, const char* _file, int _line, const char* _call, const char* _message)
		{
			if (_ret == S_OK || _ret == S_FALSE)
				return _ret;
			else
			{
				std::stringstream sstr;
				sstr << "Micosoft call failed: (" << _call << ")" << std::endl <<
					"\tfile: " << _file << std::endl <<
					"\tline: " << _line << std::endl <<
					"\treturn code:" << Engine::ErrorHandler::hresult_to_string(_ret) << std::endl <<
					"\tmessage:" << _message << std::endl;
				throw std::runtime_error(sstr.str().c_str());
			}
		}
	}
}

#define dx_except_m(ret, message) Engine::ErrorHandler::dx_Error_Message(ret, __FILE__, __LINE__, #ret, message);
#define dx_except(ret) Engine::ErrorHandler::dx_Error_Message(ret, __FILE__, __LINE__, #ret);