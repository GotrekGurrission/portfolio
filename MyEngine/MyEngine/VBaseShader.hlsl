#pragma pack_matrix( row_major )

StructuredBuffer<float4x4> worldMatrices : register(t0);

cbuffer ObjectData : register(b0)
{
    int transformID;
    int materialID;
};

cbuffer CameraData : register(b1)
{
    float4x4 view;
    float4x4 projection;
    float3 cameraPosition;
    float pad;
};

struct VS_INPUT
{
    float4 position : POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float2 uv : TEXCOORD;
    float4 color : COLOR;
};

struct VS_OUTPUT
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
    float3x3 tbn : TANGENT;
    float2 uv : TEXCOORD;
    float4 color : COLOR;
    float4 viewPostion : VIEW;
};


VS_OUTPUT main(VS_INPUT _input)
{
    //Mit MaterialData in Vertex Shader anlegen
    float2 tiling = float2(10,10);
    float2 offset = float2(0,0);
    //Every Matrix from a structures Buffer must be transpose (Bug in StructuredBuffer ?)
    float4x4 trans = transpose(worldMatrices[transformID]);
    float4x4 mv = mul(trans, view);
    //float4x4 mv = mul(worldMatrices[transformID], view);
    
    VS_OUTPUT output;
    
    output.viewPostion = mul(float4(_input.position.xyz, 1.0f), mv);
    output.position = mul(mul(float4(_input.position.xyz, 1.0f), mv), projection);
    //output.position = mul(float4(_input.position.xyz, 1.0f), mul(view, projection));
    
    float3 normal = normalize(mul(float4(_input.normal, 0), mv));
    float3 tangent = normalize(mul(float4(_input.tangent, 0), mv));
    //Gramische 
    tangent = normalize(tangent - dot(tangent, normal) * normal);
    output.normal = normal;
    output.tbn[0] = tangent;
    output.tbn[1] = cross(tangent, normal);
    output.tbn[2] = normal;
    //output.tbn = transpose(output.tbn);
    output.uv = (_input.uv * tiling) + offset;
    output.color = _input.color;
    
    return output;
}