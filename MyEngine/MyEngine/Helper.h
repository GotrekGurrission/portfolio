#pragma once
#include <type_traits>

namespace Engine 
{
	template<typename enumtype>
	constexpr auto enumv(enumtype _type) { return static_cast<std::underlying_type_t<enumtype>>(_type); }
}