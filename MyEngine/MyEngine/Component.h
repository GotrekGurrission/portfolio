#pragma once
#include <memory>
#include "ComponentData.h"
#include "EngineConfig.h"

namespace Engine 
{
	class GameObject;
	namespace Component 
	{
		class Component
		{
		public:
			Component();
			virtual ~Component();

			virtual void Init();
			virtual void Update(const Time::FrameTime& _time, GameObject& _gameObject) {}

			virtual ComponentType Type() const = 0;
		};
	}
}