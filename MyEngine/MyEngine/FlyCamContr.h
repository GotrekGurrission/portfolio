#pragma once
#include "Component.h"

namespace Engine 
{
	namespace Component 
	{
		class FlyCamContr : public Component
		{
		public:
			FlyCamContr();
			~FlyCamContr();

			void Update(const Time::FrameTime& _time, GameObject& _gameObject) override;

			virtual ComponentType Type() const override { return ComponentType::Scripts; };
		};
	}
}