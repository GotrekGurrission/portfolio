#pragma once
#include <vector>
#include "EngineConfig.h"

namespace Engine 
{
	namespace Resource 
	{
		class ResourceID_Generator 
		{
		public:
			ResourceID_Generator() : CurrentMaxID(0), OpenIDs() {}
			resource_id CurrentMaxID;
			std::vector<resource_id> OpenIDs;

			resource_id GetNewID() 
			{
				if (OpenIDs.size() > 0) {
					resource_id tempID = OpenIDs.back();
					OpenIDs.pop_back();
					return tempID;
				}
				return CurrentMaxID++;
			};
			void ReleaseID(resource_id _id) { OpenIDs.push_back(_id); };

			void Reset() { CurrentMaxID = 0; OpenIDs.clear(); }
		};
	}
}