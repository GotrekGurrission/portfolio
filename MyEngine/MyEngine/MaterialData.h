#pragma once
#include <SimpleMath.h>
#include "EngineConfig.h"

namespace Engine 
{
	namespace Data 
	{
		class MaterialData 
		{
		public:
			Resource::resource_id Shader;
			std::array<Resource::resource_id, Resource::maxMatTexturesCount> Textures{0,0,0,0};

			std::array<DirectX::SimpleMath::Vector4, Resource::maxParamCount> Params;
		};
	}
}