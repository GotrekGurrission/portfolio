#include "ObjectGraph.h"
using namespace Engine::Resource;

Engine::Resource::ObjectGraph::ObjectGraph() : idGenerator()
{
	CreateRootNode();
}

Engine::Resource::ObjectGraph::~ObjectGraph()
{
}

resource_id Engine::Resource::ObjectGraph::CreateNode(resource_id _parent)
{
	//Clear();
	ObjectNode tempNode = ObjectNode(idGenerator.GetNewID(), _parent);
	nodes.insert_or_assign(tempNode.ID, tempNode);
	root = tempNode.ID;
	return tempNode.ID;
}

resource_id Engine::Resource::ObjectGraph::CreateRootNode()
{
	Clear();
	ObjectNode tempNode = ObjectNode(idGenerator.GetNewID(), invalidID);
	nodes.insert_or_assign(tempNode.ID, tempNode);
	root = tempNode.ID;
	return tempNode.ID;
}

void Engine::Resource::ObjectGraph::Clear()
{
	nodes.clear();
	root = invalidID;
	idGenerator.Reset();
}

void Engine::Resource::ObjectGraph::AttachChild(resource_id _parent, resource_id _child)
{
	auto& parent = nodes.at(_parent);
	auto& child = nodes.at(_child);

	if (parent.ChildCount > 0) {
		child.Brother = parent.FirstChild;
	}
	parent.FirstChild = child.ID;
	parent.ChildCount++;
}

#pragma region ResourceNode
Engine::Resource::ObjectNode::ObjectNode() :
	ID(invalidID),
	Parent(invalidID),
	Brother(invalidID),
	FirstChild(invalidID),
	ChildCount(0)
{
}

Engine::Resource::ObjectNode::ObjectNode(resource_id _id, resource_id _parent) :
	ID(_id),
	Parent(_parent),
	Brother(invalidID),
	FirstChild(invalidID),
	ChildCount(0)
{
}

Engine::Resource::ObjectNode::ObjectNode(resource_id _id) :
	ID(_id),
	Parent(invalidID),
	Brother(invalidID),
	FirstChild(invalidID),
	ChildCount(0)
{
}
#pragma endregion