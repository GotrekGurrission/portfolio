#pragma once
#include <memory>
#include "WindowData.h"
#include "Window.h"
#include "RenderCore.h"
#include "SimpleRenderPass.h"
#include "ResourceManager.h"
#include "TestLevel.h"
#include "EngineConfig.h"

namespace Engine 
{
	class Core : Window
	{
	public:
		Core(Engine::Data::WindowData _windowData);

		void InitGameloop();
		void Update(const Time::FrameTime& _time = {});
		void Render(const Data::RenderPassData& _renderPassData);

	private:
		Data::RenderCoreData renderCoreData;
		std::unique_ptr<Renderer::RenderCore> renderCore;
		std::shared_ptr<Engine::Data::ComponentData> componentData;
		std::unique_ptr<Engine::Resource::ResourceManager> ressourceManager;
		std::unique_ptr<Engine::Renderer::SimpleRenderPass> renderPass;
		Engine::Level::TestLevel testLevel = Engine::Level::TestLevel();
	};
}