#pragma once
#include<string>

namespace Engine 
{
	namespace Shader 
	{
		class Texture 
		{
		public:
			Texture() {};
			Texture(const std::wstring& _fileName) : FileName(_fileName){}

			std::wstring FileName;
		};
	}
}