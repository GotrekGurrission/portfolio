#pragma once
#include <d3d11_4.h>
#include <dxgi1_5.h>

namespace Engine 
{
	namespace Data 
	{
		struct RenderCoreData
		{
			HWND WindowHandler;

			ID3D11Device5* Device;
			ID3D11DeviceContext4* DeviceContext;

			int Width;
			int Height;

			bool Windowed;
		};
	}
}