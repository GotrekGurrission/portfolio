#include "Window.h"

LRESULT CALLBACK WndProc(HWND _hWnd, UINT _msg, WPARAM _wParam, LPARAM _lParam);

Engine::Window::Window(Data::WindowData _windowData)
{
	keyboard = std::make_unique<DirectX::Keyboard>();
	WindowData = _windowData;

	WNDCLASS wc = {};
	wc.hInstance = WindowData.HInstance;
	wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_BACKGROUND);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.lpszClassName = WindowData.Name;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;

	if (!RegisterClass(&wc)) 
	{
		int x = 0;
	} //ErrorMessage("CE_Window", 1, 20, "Erstellen des WNDCLASS fehlgeschlagen");

	RECT rect = { (INT)50, (INT)50, WindowData.Width, WindowData.Height };
	DWORD style = WS_OVERLAPPEDWINDOW;
	AdjustWindowRect(&rect, style, false);

	WindowData.WindowHandler = CreateWindow(wc.lpszClassName,
		wc.lpszClassName,
		style, rect.left,
		rect.top,
		rect.right - rect.left,
		rect.bottom - rect.top,
		nullptr, nullptr,
		WindowData.HInstance, nullptr);

	//TODO: Error
	if (!WindowData.WindowHandler) {} //ErrorMessage("CE_Window", 2, 35, "Erstellen des WindowHandlers ist fehlgeschlagen");

	ShowWindow(WindowData.WindowHandler, WindowData.NShowCmd);
	SetFocus(WindowData.WindowHandler);
}

Engine::Window::~Window()
{
	if (WindowData.WindowHandler != NULL)
	{
		DestroyWindow(WindowData.WindowHandler);
	}
}

bool Engine::Window::IsRun()
{
	MSG msg = {};
	if (PeekMessage(&msg, nullptr, 0, UINT_MAX, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if (msg.message == WM_QUIT) return false;
	}
	return true;
}

LRESULT CALLBACK WndProc(HWND _hWnd, UINT _msg, WPARAM _wParam, LPARAM _lParam) {
	switch (_msg) {
	case WM_ACTIVATEAPP:
		DirectX::Keyboard::ProcessMessage(_msg, _wParam, _lParam);
		return 0;
	case WM_KEYDOWN:
		DirectX::Keyboard::ProcessMessage(_msg, _wParam, _lParam);
		if (_wParam == VK_ESCAPE) {
			DestroyWindow(_hWnd); //Ist nur Tempor�r
		}
		return 0;
	case WM_KEYUP:
		DirectX::Keyboard::ProcessMessage(_msg, _wParam, _lParam);
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(_hWnd, _msg, _wParam, _lParam);
}
