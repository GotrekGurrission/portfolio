#pragma once
#include <array>
#include <d3d11_4.h>
#include <dxgi1_5.h>
#include <cstdint>
#include "Helper.h"
#include <filesystem>
#include <unordered_map>
#include <chrono>
#include <SimpleMath.h>

namespace fs = std::filesystem;
namespace Engine
{
	namespace Config
	{
		inline constexpr auto FeatureLevels = std::array{
					D3D_FEATURE_LEVEL_10_0
					//D3D_FEATURE_LEVEL_10_1, // Hier noch nciht sicher ob die allenoch funktionieren
					//D3D_FEATURE_LEVEL_11_0,
					//D3D_FEATURE_LEVEL_11_1,
					//D3D_FEATURE_LEVEL_12_0,
					//D3D_FEATURE_LEVEL_12_1,
					//D3D_FEATURE_LEVEL_12_2
		};
	}

	template<typename K, typename V>
	inline bool Contains(const std::unordered_map<K, V>& _map, K _key) {
		return _map.find(_key) != _map.end();
	}

	namespace Resource 
	{
		using resource_id = std::uint32_t;
		inline constexpr resource_id invalidID = ~0u;

		inline constexpr int maxParamCount = 8;
		inline constexpr int maxAllTexturesCount = 64;
		inline constexpr int maxMatTexturesCount = 4;
		inline constexpr int maxDirectionalLight = 4;
		inline constexpr int maxPointLight = 8;
		inline constexpr int maxSpotLight = 8;
	
		//inline const fs::path ShaderDirectory = "Assets/Shaders";
		inline const fs::path ShaderDirectory = "";
		inline const fs::path TextureDirectory = "Assets/Textures";
		//inline const fs::path TextureDirectory = "";
	}

	namespace Time 
	{
		using time_t = std::chrono::duration<float, std::ratio<1, 1>>;
		
		struct FrameTime {
			time_t dt;
			time_t t;
		};
	}

	namespace Math_t {
		inline constexpr float degreeRadians = DirectX::XM_PI / 180.0f;
		inline constexpr float radianToDegree = 180.0f / DirectX::XM_PI;
	}

	using ComponentMask = std::uint64_t;
	namespace Component {
		enum class ComponentType : ComponentMask
		{
			Transform			= 1 << 0,
			MeshRenderer		= 1 << 1,
			Material			= 1 << 2,
			Camera				= 1 << 3,
			DirectionalLight	= 1 << 4,
			PointLight			= 1 << 5,
			SpotLight			= 1 << 6,
			EnviromentLight		= 1 << 7,
			Scripts				= 1 << 8
		};
	}
}