#pragma once
#include "WindowData.h"
#include "RenderCoreData.h"

namespace Engine 
{
	namespace Data 
	{
		struct ComponentData
		{
		public:
			Engine::Data::WindowData* WindowData;
			Engine::Data::RenderCoreData* RenderCoreData;
		};
	}
}