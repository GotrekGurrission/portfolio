#include "Core.h"

Engine::Core::Core(Engine::Data::WindowData _windowData) :
	Window(_windowData),
	renderCoreData()
{
	renderCoreData.WindowHandler = WindowData.WindowHandler;
	renderCoreData.Width = 1280;
	renderCoreData.Height = 720;
	renderCoreData.Windowed = true;

	renderCore = std::make_unique<Renderer::RenderCore>(&renderCoreData);

	componentData = std::make_shared<Engine::Data::ComponentData>();
	componentData->WindowData = &WindowData;
	componentData->RenderCoreData = &renderCoreData;

	ressourceManager = std::make_unique<Engine::Resource::ResourceManager>(componentData);
	ressourceManager->LoadLevel(testLevel);

	renderPass = std::make_unique<Engine::Renderer::SimpleRenderPass>(renderCoreData);
}

void Engine::Core::InitGameloop()
{
	Data::RenderPassData rpData{};
	rpData.ressourceManager = ressourceManager.get();

	Update();

	renderPass->BuildRenderpass(rpData);
	using higherTime = std::chrono::high_resolution_clock::duration;
	higherTime currentTime(0);
	const auto startTime = std::chrono::high_resolution_clock::now();
	while (true)
	{
		auto newTime = std::chrono::high_resolution_clock::now() - startTime;
		auto delta = newTime - currentTime;
		currentTime = newTime;

		Time::FrameTime frameTime{std::chrono::duration_cast<Time::time_t>(delta), std::chrono::duration_cast<Time::time_t>(currentTime)};

		if (!IsRun()) {
			break;
		}

		Update(frameTime);

		Render(rpData);
	}
}

void Engine::Core::Update(const Time::FrameTime& _time)
{
	ressourceManager->Update(_time);
}

void Engine::Core::Render(const Data::RenderPassData& _renderPassData)
{
	renderCore->BeginScene(0.1f, 0, 0);

	renderPass->UpdateData(_renderPassData);

	renderPass->Render(_renderPassData);

	renderCore->EndScene();
}