#pragma once
#include <vector>
#include <filesystem>
#include "ObjectGraph.h"
#include "ResourceMap.h"
#include "GameObject.h"
#include "MeshData.h"
#include "ComponentData.h"
#include "BaseLevel.h"
#include "ErrorHandler.h"
#include "Shader.h"
#include "MaterialData.h"
#include "Texture.h"

namespace Engine 
{
	namespace Resource 
	{
		class ResourceManager
		{
		public:
			ResourceManager(std::shared_ptr<Engine::Data::ComponentData> _componentData);
			~ResourceManager();

			void LoadLevel(Engine::Level::BaseLevel& _level);
			void ClearLevel();

			void Update(const Time::FrameTime& _time);
			void Render();

			GameObject& CreateGameObject(Engine::Resource::resource_id _parent = 0);

			const std::vector<Engine::GameObject>& AllGameObjects() const { return gameObjects; }
		private:
			std::shared_ptr<Engine::Data::ComponentData> ComponentData;

#pragma region GameObjects
			std::vector <Engine::GameObject> gameObjects;
			Engine::Resource::ObjectGraph gameObjectGraph;

			GameObject& CreateRootGameObject();
#pragma endregion

#pragma region Resources
		public:
			Engine::Resource::resource_id CreateMesh(const std::string& _name);
			Engine::Resource::resource_id CreateShader(const std::string& _name);
			Engine::Resource::resource_id CreateMaterial(const std::string& _name);
			Engine::Resource::resource_id CreateTexture(const std::string& _name);

			ResourceMap<std::shared_ptr<Data::MeshData>> meshes;
			ResourceMap<std::unique_ptr<Shader::Shader>> shaders;
			ResourceMap<std::unique_ptr<Data::MaterialData>> materials;
			ResourceMap<std::unique_ptr<Shader::Texture>> textures;
#pragma endregion
		};
	}
}