#include "FlyCamContr.h"
#include "GameObject.h"
#include "Keyboard.h"

Engine::Component::FlyCamContr::FlyCamContr()
{
}

Engine::Component::FlyCamContr::~FlyCamContr()
{
}

void Engine::Component::FlyCamContr::Update(const Time::FrameTime& _time, GameObject& _gameObject)
{
	float movementSpeed = 2.0f;
	float rotationSpeed = 60.0f;
	auto kb = DirectX::Keyboard::Get().GetState();

	if (kb.W)
		_gameObject.Transform->Position += _gameObject.Transform->LocalForward * movementSpeed * _time.dt.count();

	if (kb.S)
		_gameObject.Transform->Position += _gameObject.Transform->LocalBackward * movementSpeed * _time.dt.count();

	if (kb.A)
		_gameObject.Transform->Position += _gameObject.Transform->LocalLeft * movementSpeed * _time.dt.count();

	if (kb.D)
		_gameObject.Transform->Position += _gameObject.Transform->LocalRight * movementSpeed * _time.dt.count();


	if (kb.Up)
		_gameObject.Transform->Eulerangle += DirectX::SimpleMath::Vector3::Right * rotationSpeed * _time.dt.count();

	if (kb.Down)
		_gameObject.Transform->Eulerangle += DirectX::SimpleMath::Vector3::Left * rotationSpeed * _time.dt.count();
	
	if (kb.Right)
		_gameObject.Transform->Eulerangle += DirectX::SimpleMath::Vector3::Down * rotationSpeed * _time.dt.count();

	if (kb.Left)
		_gameObject.Transform->Eulerangle += DirectX::SimpleMath::Vector3::Up * rotationSpeed * _time.dt.count();
}