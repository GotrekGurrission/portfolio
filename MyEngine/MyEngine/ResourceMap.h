#pragma once
#include <unordered_map>
#include "ResourceHelper.h"

namespace Engine 
{
	namespace Resource 
	{
		template<typename resourceType>
		class ResourceMap
		{
		public:
			ResourceMap();
			resource_id Add(const resourceType& _resource, const std::string& _name = "");
			resourceType& Add_t(const resourceType& _resource, const std::string& _name = "");
			
			resource_id Add(resourceType&& _resource, const std::string& _name = "");
			resourceType& Add_t(resourceType&& _resource, const std::string& _name = "");

			resourceType& Get(resource_id _id);
			resourceType& Get(const std::string& _name);

			resource_id GetID(const std::string& _name);

			void Delete(resource_id _id);
			void Delete(const std::string& _name);
		private:
			ResourceID_Generator idGenerator;
			std::unordered_map<resource_id, resourceType> resources;
			std::unordered_map<std::string, resource_id> namesToResource;
			std::unordered_map<resource_id, std::string> resourcesToName;
		};

#pragma region Definition
		template<typename resourceType>
		inline ResourceMap<resourceType>::ResourceMap() :
			idGenerator(),
			resources(),
			namesToResource(),
			resourcesToName()
		{	
		}
		template<typename resourceType>
		inline resource_id ResourceMap<resourceType>::Add(const resourceType& _resource, const std::string& _name)
		{
			resource_id tempID = idGenerator.GetNewID();
			resources.insert_or_assign(tempID, _resource);
			if (!_name.empty()) {
				namesToResource.insert_or_assign(_name, tempID);
				resourcesToName.insert_or_assign(tempID, _name);
			}
			return tempID;
		}
		template<typename resourceType>
		inline resourceType& ResourceMap<resourceType>::Add_t(const resourceType& _resource, const std::string& _name)
		{
			return Get(Add(_resource, _name));
		}
		template<typename resourceType>
		inline resource_id ResourceMap<resourceType>::Add(resourceType&& _resource, const std::string& _name)
		{
			resource_id tempID = idGenerator.GetNewID();
			resources.insert_or_assign(tempID, std::move(_resource));
			if (!_name.empty()) {
				namesToResource.insert_or_assign(_name, tempID);
				resourcesToName.insert_or_assign(tempID, _name);
			}
			return tempID;
		}
		template<typename resourceType>
		inline resourceType& ResourceMap<resourceType>::Add_t(resourceType&& _resource, const std::string& _name)
		{
			return Get(Add(std::move(_resource), _name));
		}
		template<typename resourceType>
		inline resourceType& ResourceMap<resourceType>::Get(resource_id _id)
		{
			return resources.at(_id);
		}
		template<typename resourceType>
		inline resourceType& ResourceMap<resourceType>::Get(const std::string& _name)
		{
			return resources.at(namesToResource.at(_name));
		}
		template<typename resourceType>
		inline resource_id ResourceMap<resourceType>::GetID(const std::string& _name)
		{
			return namesToResource.at(_name);
		}
		template<typename resourceType>
		inline void ResourceMap<resourceType>::Delete(resource_id _id)
		{
			resources.erase(_id);
			if (resourcesToName.find(_id) != resourcesToName.end()) 
			{
				namesToResource.erase(resourcesToName.at(_id));
				resourcesToName.erase(_id);
			}
		}
		template<typename resourceType>
		inline void ResourceMap<resourceType>::Delete(const std::string& _name)
		{
			Delete(namesToResource.at(_name));
		}
#pragma endregion
	}
}
