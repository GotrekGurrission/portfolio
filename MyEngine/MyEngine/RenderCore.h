#pragma once
#include "RenderCoreData.h"
#include "EngineConfig.h"
#include <d3d11_4.h>
#include <dxgi1_5.h>

#include <wrl/client.h>

#pragma comment(lib, "d3d11.lib")

namespace Engine 
{
	namespace Renderer 
	{
		class RenderCore
		{
		public:
			RenderCore(Data::RenderCoreData* _renderCoreData);
			~RenderCore();

			void BeginScene(float _rgb);
			void BeginScene(float _r, float _g, float _b);
			void EndScene();

			Data::RenderCoreData* RenderCoreData;
		private:
			Microsoft::WRL::ComPtr<ID3D11Device5> device;
			Microsoft::WRL::ComPtr<ID3D11DeviceContext4> deviceContext;

			Microsoft::WRL::ComPtr<IDXGISwapChain1> swapChain;
			Microsoft::WRL::ComPtr<ID3D11Texture2D> backBuffer;

			//Tiefenbuffer
			Microsoft::WRL::ComPtr<ID3D11Texture2D1> depthStencilTexture;
			Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depthStencilView;
			Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depthStencilState;

			//Blendbuffer (Transparent)
			Microsoft::WRL::ComPtr<ID3D11BlendState1> blendState = nullptr;

			//Culling
			Microsoft::WRL::ComPtr<ID3D11RasterizerState2> rasterizerState;

			//ComPtr<ID3D11RenderTargetView1> renderTargetView;
			Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTargetView;
			D3D11_VIEWPORT viewPort = {};

			void CreateDevices(Data::RenderCoreData* _renderCoreData);
			void CreateSwapChain(Data::RenderCoreData* _renderCoreData);
			void CreateDepthbuffer(Data::RenderCoreData* _renderCoreData);
			void CreateRasterizeState(Data::RenderCoreData* _renderCoreData);
			void CreateBlendState(Data::RenderCoreData* _renderCoreData);
			void CreateViewport(Data::RenderCoreData* _renderCoreData);
		};
	}
}