#include "TestLevel.h"
#include "Camera.h"
#include "MeshRenderer.h"
//#include "Material.h"
#include "ResourceManager.h"
#include "FlyCamContr.h"

#include "DirectionalLight.h"
#include "PointLight.h"

void Engine::Level::TestLevel::Load(Engine::Resource::ResourceManager* _ressourceManager)
{
	Engine::GameObject& camera = _ressourceManager->CreateGameObject();
	camera.AddComponent(new Engine::Component::Camera());
	camera.AddComponent(new Engine::Component::FlyCamContr());
	camera.Transform->Position = { 0,1,0 };

	auto cubeID = _ressourceManager->CreateMesh("Cube");
	Data::MeshData::GetPrimitiveCube(_ressourceManager->meshes.Get(cubeID), { 1,1,1,1 });

	auto planeID = _ressourceManager->CreateMesh("Plane");
	Data::MeshData::GetPrimitivePlane(_ressourceManager->meshes.Get(planeID), { 1,1,1,1 });

#pragma region Create Textures
	auto defaultTexture = _ressourceManager->CreateTexture("DefaultTex.jpg");

	//Barkbrown
	auto barkbrown_diff = _ressourceManager->CreateTexture("barkbrown/bark_brown_diff.jpg");
	auto barkbrown_disp = _ressourceManager->CreateTexture("barkbrown/bark_brown_disp.png");
	auto barkbrown_nor_gl = _ressourceManager->CreateTexture("barkbrown/bark_brown_nor_gl.png");
	auto barkbrown_rough = _ressourceManager->CreateTexture("barkbrown/bark_brown_rough.jpg");

	auto greenmetalrust_diff = _ressourceManager->CreateTexture("greenmetalrust/green_metal_rust_diff.jpg");
	auto greenmetalrust_disp = _ressourceManager->CreateTexture("greenmetalrust/green_metal_rust_disp.png");
	auto greenmetalrust_nor_gl = _ressourceManager->CreateTexture("greenmetalrust/green_metal_rust_nor_gl.png");
	auto greenmetalrust_rough = _ressourceManager->CreateTexture("greenmetalrust/green_metal_rust_rough.jpg");

	auto hexagonal_diff = _ressourceManager->CreateTexture("hexagonalconcretepaving/hexagonal_diff.jpg");
	auto hexagonal_disp = _ressourceManager->CreateTexture("hexagonalconcretepaving/hexagonal_disp.png");
	auto hexagonal_nor_gl = _ressourceManager->CreateTexture("hexagonalconcretepaving/hexagonal_nor_gl.png");
	auto hexagonal_rough = _ressourceManager->CreateTexture("hexagonalconcretepaving/hexagonal_rough.png");

	auto laminatefloor_diff = _ressourceManager->CreateTexture("laminatefloor/laminate_floor_diff.jpg");
	auto laminatefloor_disp = _ressourceManager->CreateTexture("laminatefloor/laminate_floor_disp.png");
	auto laminatefloor_nor_gl = _ressourceManager->CreateTexture("laminatefloor/laminate_floor_nor_gl.png");
	auto laminatefloor_rough = _ressourceManager->CreateTexture("laminatefloor/laminate_floor_rough.png");

	auto slatedriveway_diff = _ressourceManager->CreateTexture("slatedriveway/slate_driveway_diff.jpg");
	auto slatedriveway_disp = _ressourceManager->CreateTexture("slatedriveway/slate_driveway_disp.png");
	auto slatedriveway_nor_gl = _ressourceManager->CreateTexture("slatedriveway/slate_driveway_nor_gl.png");
	auto slatedriveway_rough = _ressourceManager->CreateTexture("slatedriveway/slate_driveway_rough.png");
#pragma endregion

	DirectX::SimpleMath::Vector4 defaultSpecularColor = { 0.04f, 0.04f, 0.04f, 1.0 };

#pragma region Create Shader
	auto defaultShader = _ressourceManager->CreateShader("BaseShader");
#pragma endregion

#pragma region Create Material
	//TODO: Shader Id beim erstellen mitgeben
	auto DefaultMaterial = _ressourceManager->CreateMaterial("DefaultMaterial");
	_ressourceManager->materials.Get(DefaultMaterial)->Shader = defaultShader;
	_ressourceManager->materials.Get(DefaultMaterial)->Textures[0] = defaultTexture;
	//Specular Color
	_ressourceManager->materials.Get(DefaultMaterial)->Params[0] = defaultSpecularColor;
	//Shinnines 
	_ressourceManager->materials.Get(DefaultMaterial)->Params[1] = { 1028, 0, 0 };

	auto BarkBrownMaterial = _ressourceManager->CreateMaterial("BarkBrownMaterial");
	_ressourceManager->materials.Get(BarkBrownMaterial)->Shader = defaultShader;
	_ressourceManager->materials.Get(BarkBrownMaterial)->Textures[0] = barkbrown_diff;
	_ressourceManager->materials.Get(BarkBrownMaterial)->Textures[1] = barkbrown_disp;
	_ressourceManager->materials.Get(BarkBrownMaterial)->Textures[2] = barkbrown_nor_gl;
	_ressourceManager->materials.Get(BarkBrownMaterial)->Textures[3] = barkbrown_rough;
	//Specular Color
	_ressourceManager->materials.Get(BarkBrownMaterial)->Params[0] = defaultSpecularColor;
	//Shinnines 
	_ressourceManager->materials.Get(BarkBrownMaterial)->Params[1] = { 256, 0, 0 };

	auto GreenMetalRustMaterial = _ressourceManager->CreateMaterial("GreenMetalRustMaterial");
	_ressourceManager->materials.Get(GreenMetalRustMaterial)->Shader = defaultShader;
	_ressourceManager->materials.Get(GreenMetalRustMaterial)->Textures[0] = greenmetalrust_diff;
	_ressourceManager->materials.Get(GreenMetalRustMaterial)->Textures[1] = greenmetalrust_disp;
	_ressourceManager->materials.Get(GreenMetalRustMaterial)->Textures[2] = greenmetalrust_nor_gl;
	_ressourceManager->materials.Get(GreenMetalRustMaterial)->Textures[3] = greenmetalrust_rough;
	//Specular Color
	_ressourceManager->materials.Get(GreenMetalRustMaterial)->Params[0] = defaultSpecularColor;
	//Shinnines 
	_ressourceManager->materials.Get(GreenMetalRustMaterial)->Params[1] = { 256, 0, 0 };

	auto HexagonalConcretePavingMaterial = _ressourceManager->CreateMaterial("HexagonalConcretePavingMaterial");
	_ressourceManager->materials.Get(HexagonalConcretePavingMaterial)->Shader = defaultShader;
	_ressourceManager->materials.Get(HexagonalConcretePavingMaterial)->Textures[0] = hexagonal_diff;
	_ressourceManager->materials.Get(HexagonalConcretePavingMaterial)->Textures[1] = hexagonal_disp;
	_ressourceManager->materials.Get(HexagonalConcretePavingMaterial)->Textures[2] = hexagonal_nor_gl;
	_ressourceManager->materials.Get(HexagonalConcretePavingMaterial)->Textures[3] = hexagonal_rough;
	//Specular Color
	_ressourceManager->materials.Get(HexagonalConcretePavingMaterial)->Params[0] = defaultSpecularColor;
	//Shinnines 
	_ressourceManager->materials.Get(HexagonalConcretePavingMaterial)->Params[1] = { 256, 0, 0 };

	auto LaminateFloorMaterial = _ressourceManager->CreateMaterial("LaminateFloorMaterial");
	_ressourceManager->materials.Get(LaminateFloorMaterial)->Shader = defaultShader;
	_ressourceManager->materials.Get(LaminateFloorMaterial)->Textures[0] = laminatefloor_diff;
	_ressourceManager->materials.Get(LaminateFloorMaterial)->Textures[1] = laminatefloor_disp;
	_ressourceManager->materials.Get(LaminateFloorMaterial)->Textures[2] = laminatefloor_nor_gl;
	_ressourceManager->materials.Get(LaminateFloorMaterial)->Textures[3] = laminatefloor_rough;
	//Specular Color
	_ressourceManager->materials.Get(LaminateFloorMaterial)->Params[0] = defaultSpecularColor;
	//Shinnines 
	_ressourceManager->materials.Get(LaminateFloorMaterial)->Params[1] = { 256, 0, 0 };

	auto SlateDrivewayMaterial = _ressourceManager->CreateMaterial("SlateDrivewayMaterial");
	_ressourceManager->materials.Get(SlateDrivewayMaterial)->Shader = defaultShader;
	_ressourceManager->materials.Get(SlateDrivewayMaterial)->Textures[0] = slatedriveway_diff;
	_ressourceManager->materials.Get(SlateDrivewayMaterial)->Textures[1] = slatedriveway_disp;
	_ressourceManager->materials.Get(SlateDrivewayMaterial)->Textures[2] = slatedriveway_nor_gl;
	_ressourceManager->materials.Get(SlateDrivewayMaterial)->Textures[3] = slatedriveway_rough;
	//Specular Color
	_ressourceManager->materials.Get(SlateDrivewayMaterial)->Params[0] = defaultSpecularColor;
	//Shinnines 
	_ressourceManager->materials.Get(SlateDrivewayMaterial)->Params[1] = { 256, 0, 0 };
#pragma endregion
	
#pragma region Create GameObjects

	GameObject& ground = _ressourceManager->CreateGameObject();
	ground.AddComponent(new Component::MeshRenderer(planeID, LaminateFloorMaterial));
	ground.Transform->Position = { 0, 0, 0 };
	ground.Transform->Scale = { 100, 1, 100 };

	GameObject& cubeA = _ressourceManager->CreateGameObject();
	cubeA.AddComponent(new Component::MeshRenderer(cubeID, SlateDrivewayMaterial));
	cubeA.Transform->Position = { 10, 4, 10 };
	cubeA.Transform->Scale = { 4, 8, 4 };

	GameObject& cubeB = _ressourceManager->CreateGameObject();
	cubeB.AddComponent(new Component::MeshRenderer(cubeID, HexagonalConcretePavingMaterial));
	cubeB.Transform->Position = { 10, 0.5f, 0 };
	cubeB.Transform->Scale = { 10, 1, 10 };

	GameObject& cubeC = _ressourceManager->CreateGameObject();
	cubeC.AddComponent(new Component::MeshRenderer(cubeID, SlateDrivewayMaterial));
	cubeC.Transform->Position = { -6, 6, 8 };
	cubeC.Transform->Scale = { 4, 4, 4 };

	GameObject& cubeD = _ressourceManager->CreateGameObject();
	cubeD.AddComponent(new Component::MeshRenderer(cubeID, LaminateFloorMaterial));
	cubeD.Transform->Position = { 9, 10, -4 };
	cubeD.Transform->Scale = { 4, 4, 4 };

	GameObject& cubeE = _ressourceManager->CreateGameObject();
	cubeE.AddComponent(new Component::MeshRenderer(cubeID, GreenMetalRustMaterial));
	cubeE.Transform->Position = { -6, 4, -5 };
	cubeE.Transform->Scale = { 4, 4, 4 };

	GameObject& cubeF = _ressourceManager->CreateGameObject();
	cubeF.AddComponent(new Component::MeshRenderer(cubeID, LaminateFloorMaterial));
	cubeF.Transform->Position = { 3, 5, 9 };
	cubeF.Transform->Scale = { 4, 4, 4 };

	GameObject& direcLight = _ressourceManager->CreateGameObject();
	direcLight.AddComponent(new Component::Light::DirectionalLight());
	direcLight.GetComponent<Component::Light::DirectionalLight>()->Color = { 1,1,1,1 };
	direcLight.GetComponent<Component::Light::DirectionalLight>()->Direction = { -1,-1,0 };

	GameObject& pointLight = _ressourceManager->CreateGameObject();
	pointLight.AddComponent(new Component::Light::PointLight());
	pointLight.GetComponent<Component::Light::PointLight>()->Color = { 1,1,1,1 };
	pointLight.GetComponent<Component::Light::PointLight>()->Range = 20;
	pointLight.Transform->Position = { 0,1,0 };

#pragma endregion
}
