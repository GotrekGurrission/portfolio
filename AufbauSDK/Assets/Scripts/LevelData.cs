using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="LevelData", menuName ="RTS/Level", order =0)]
public class LevelData : ScriptableObject
{
    public int LifePoint;
}
