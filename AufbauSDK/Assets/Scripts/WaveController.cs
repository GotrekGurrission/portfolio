using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Grid;

public class WaveController : MonoBehaviour
{
    public GridInspector gridInspector;

    public List<WaveData> waves = new List<WaveData>();
    private int waveCount = 0;
    private WaveData CurrentWave { get { return waves[waveCount]; } }

    [SerializeField]
    private float spawnTime;
    [SerializeField]
    private float unitSpawnTime;
    [SerializeField]
    private int unitCount;

    public Transform SpawnPoint;
    public Transform TargetPoint;



    private void Update()
    {
        if(waveCount < waves.Count && spawnTime > CurrentWave.SpawnTime)
        {
            if(unitCount < CurrentWave.UnitCount)
            {
                if (unitSpawnTime > CurrentWave.UnitSpawnTime)
                {
                    Enemy tempEnemy = Instantiate(waves[waveCount].UnitPrefab, SpawnPoint.position, Quaternion.identity).GetComponent<Enemy>();

                    tempEnemy.Init(gridInspector.Grid);
                    tempEnemy.MoveTo(TargetPoint.position);

                    unitSpawnTime = 0;
                    unitCount++;
                }
                else
                {
                    unitSpawnTime += Time.deltaTime;
                }
            }
            else
            {
                unitCount = 0;
                spawnTime = 0;
                waveCount++;
            }
        }
        else
        {
            spawnTime += Time.deltaTime;
        }
    }
}
