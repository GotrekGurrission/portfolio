using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="WaveData", menuName = "RTS/WaveSystem/Wave", order = 0)]
public class WaveData : ScriptableObject
{
    [SerializeField]
    private GameObject unitPrefab;
    [SerializeField]
    private int unitCount;
    /// <summary>
    /// SpawnTime for the Wave
    /// </summary>
    [SerializeField]
    private float spawnTime;
    /// <summary>
    /// Time between the Spawn from Units
    /// </summary>
    [SerializeField]
    private float unitSpawnTime;

    public GameObject UnitPrefab { get { return unitPrefab; } }
    public int UnitCount { get { return unitCount; } }
    public float SpawnTime { get { return spawnTime; } }
    public float UnitSpawnTime { get { return unitSpawnTime; } }
}