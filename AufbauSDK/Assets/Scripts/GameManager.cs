using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Grid;
using Rts.Unit;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private LevelData levelData;
    private int currentLifePoints = 0;
    public int LifePoint
    {
        get
        {
            return currentLifePoints;
        }
        set
        {
            currentLifePoints = value;
            if(currentLifePoints <= 0)
            {
                GameOver();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentLifePoints = levelData.LifePoint;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GameOver()
    {
        Debug.Log("Gameover");
        //TODO: Implement Game Over
    }
}
