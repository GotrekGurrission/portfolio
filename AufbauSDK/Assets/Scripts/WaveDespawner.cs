using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveDespawner : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;
    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<Enemy>(out Enemy _enemy))
        {
            gameManager.LifePoint -= _enemy.Data.Lifecount;
            Destroy(_enemy.gameObject, 0.1f);
        }
    }
}
