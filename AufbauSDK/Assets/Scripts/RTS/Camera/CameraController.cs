using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Rts.Camera
{
    public class CameraController : MonoBehaviour
    {
        private GameInput gameInput;

        [Header("Movement Settings")]
        private InputAction movment;
        public float MovementSpeed = 4.0f;
        public float ZoomMovementFaktor = 0.5f;

        [Header("Zoom Settings")]
        private InputAction zoom;
        public float ZoomSpeed;
        public float minHeight = 10;
        public float maxHeight = 20;
        public float currentZoom = 0;


        private void Awake()
        {
            BindInput();
        }
        private void Update()
        {
            Move(movment.ReadValue<Vector2>());

            ZoomCamera(zoom.ReadValue<Vector2>().y);
        }

        private void BindInput()
        {
            gameInput = new GameInput();
            movment = gameInput.Player.CameraMovement;
            zoom = gameInput.Player.CameraZoom;
        }

        private void Move(Vector2 _movDir)
        {
            transform.position += new Vector3(_movDir.x, 0, _movDir.y) * (MovementSpeed + (currentZoom*ZoomMovementFaktor)) * Time.deltaTime;
        }

        private void ZoomCamera(float _value)
        {
            currentZoom += _value * ZoomSpeed * Time.deltaTime;
            currentZoom = Mathf.Clamp(currentZoom, 0, maxHeight - minHeight);

            transform.position = new Vector3(transform.position.x, minHeight + currentZoom, transform.position.z);
        }

        private void OnEnable()
        {
            if(gameInput != null)
                gameInput.Enable();
        }

        private void OnDisable()
        {
            gameInput.Disable();
        }
    }
}