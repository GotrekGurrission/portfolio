using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building<BuildData> : MonoBehaviour where BuildData : BuildingData
{
    public BuildData Data;
}
