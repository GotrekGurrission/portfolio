using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : Building<TowerData>
{
    private List<Enemy> enemyInRange = new List<Enemy>();

    private float currentAttackSpeed = 0;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SphereCollider>().radius = Data.Range;
    }

    // Update is called once per frame
    void Update()
    {
        Attack();
    }

    public void Attack()
    {
        currentAttackSpeed += Time.deltaTime;

        if(enemyInRange.Count > 0 && currentAttackSpeed >= Data.AttackSpeed)
        {
            Enemy targetEnemy = enemyInRange[0];
            foreach(Enemy enemy in enemyInRange)
            {
                if(targetEnemy.Agent.RestPath() > enemy.Agent.RestPath())
                {
                    targetEnemy = enemy;
                }
            }

            targetEnemy.Hitpoint -= Data.Damage;
            currentAttackSpeed = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if(other.TryGetComponent<Enemy>(out Enemy _enemy))
        {
            enemyInRange.Add(_enemy);
            _enemy.OnDestroyAction.AddListener(RemoveEnemy);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Enemy>(out Enemy _enemy))
        {
            if (enemyInRange.Contains(_enemy))
            {
                enemyInRange.Remove(_enemy);
            }
        }
    }

    public void RemoveEnemy(Enemy _enemy)
    {
        if (enemyInRange.Contains(_enemy))
        {
            enemyInRange.Remove(_enemy);
        }
    }
}
