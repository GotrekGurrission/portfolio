using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tower", menuName = "RTS/Tower")]
public class TowerData : BuildingData
{
    public int Damage;
    public float Range;
    public float AttackSpeed = 1;
}
