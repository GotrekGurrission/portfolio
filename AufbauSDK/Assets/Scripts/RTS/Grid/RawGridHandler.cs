using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Grid.Data;


namespace Rts.Grid
{
    public class RawGridHandler<GridData> where GridData : RawGridData
    {
        protected GridData data;

        public RawGridHandler(GridData data)
        {
            this.data = data;
            CreateGrid();
        }

        protected virtual void CreateGrid()
        {
            if (data.Anchor == null)
            {
                data.Anchor = new GameObject(data.Name).transform;
            }
            else
            {
                data.Anchor.name = data.Name;
            }

            data.WorldPositions = new Vector3[data.Width * data.Height];

            for (int y = 0; y < data.Height; y++)
            {
                for (int x = 0; x < data.Width; x++)
                {
                    CreateNode(y * data.Width + x, x, y);
                }
            }
        }

        protected virtual void CreateNode(int _id, int _x, int _y)
        {
            data.WorldPositions[_id] = data.Anchor.TransformPoint(
                        new Vector3(
                            _x * data.Size + data.Size * 0.5f,
                            0,
                            _y * data.Size + data.Size * 0.5f));
        }

        /// <summary>
        /// Get the ID from the Node at the Worldposition
        /// </summary>
        /// <param name="_worldPosition"></param>
        /// <returns>Return Node ID or -1 if no Node found</returns>
        public int GetNodeID(Vector3 _worldPosition)
        {
            Vector3 localPosition = data.Anchor.InverseTransformPoint(_worldPosition);
            int width = (int)(localPosition.x / data.Size);
            int height = (int)(localPosition.z / data.Size);
            if (width >= 0 && width < data.Width && height >= 0 && height < data.Height)
            {
                return GetNodeID(width, height);
            }
            else
            {
                //Debug.LogWarning("No Node at the Position " + _worldPosition);
                return -1;
            }
        }

        /// <summary>
        /// Get the ID from the Node at this Grid Position
        /// </summary>
        /// <param name="_x"></param>
        /// <param name="_y"></param>
        /// <returns></returns>
        public int GetNodeID(Vector2Int _gridPosition)
        {
            return GetNodeID(_gridPosition.x, _gridPosition.y);
        }

        /// <summary>
        /// Get the ID from the Node at this Grid Position
        /// </summary>
        /// <param name="_x"></param>
        /// <param name="_y"></param>
        /// <returns></returns>
        public int GetNodeID(int _x, int _y)
        {
            return _y * data.Width + _x;
        }
        /// <summary>
        /// Get the Position from the Node in the Grid
        /// </summary>
        /// <param name="_id">Need the ID from the Node</param>
        /// <returns></returns>
        public Vector2Int GetGridPosition(int _id)
        {
            return new Vector2Int((int)_id % data.Width, (int)_id / data.Width);
        }

        /// <summary>
        /// Get the WorldPosition from the Node
        /// </summary>
        /// <param name="_id">Need the ID from the Node</param>
        /// <returns></returns>
        public Vector3 GetWorldPosition(int _id)
        {
#if UNITY_EDITOR
            if (data.WorldPositions.Length > _id && _id >= 0)
#endif
                return data.WorldPositions[_id];
#if UNITY_EDITOR
            else
                return Vector3.zero;
#endif
        }
    }
}