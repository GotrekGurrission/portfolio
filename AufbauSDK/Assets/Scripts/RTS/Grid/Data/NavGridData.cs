using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rts.Grid.Data
{
    [CreateAssetMenu(fileName ="NaviGrid", menuName ="RTS/Grid/NaviGrid", order =0)]
    public class NavGridData : RawGridData
    {
        [HideInInspector]
        public int[] NodeCost;

        public float MinWorldHeight;
        public float MaxWorldHeight;

        public LayerMask GroundLayer;
        public LayerMask BlockedLayer;
        public LayerMask BuildableLayer;

        [HideInInspector]
        public FieldStatus[] Status;
        public enum FieldStatus
        {
            None=0,
            Blocked = 1,
            Buildable = 2
        }
    }
}