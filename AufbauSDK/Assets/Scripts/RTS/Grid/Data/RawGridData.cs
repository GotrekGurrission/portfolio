using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rts.Grid.Data
{
    public class RawGridData : ScriptableObject
    {
        public string Name;

        public int Width = 0;
        public int Height = 0;
        public float Size = 1.0f;

        public Transform Anchor = null;

        [HideInInspector]
        public Vector3[] WorldPositions;
    }
}