using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Grid.Data;

namespace Rts.Grid
{
    public class GridInspector : MonoBehaviour
    {
        [SerializeField]
        private bool drawGizmo = false;

        [SerializeField]
        private NavGridData gridData;

        //private Grid<GridNode> grid = null;
        private NaviGridHandler grid;
        public NaviGridHandler Grid
        {
            get 
            {
                if (grid == null)
                    CreateGrid();
                return grid; 
            }
        }

        private void Awake()
        {
            CreateGrid();
            
        }

        private void Start()
        {
        }

        [ContextMenu("Create Grid")]
        public void CreateGrid()
        {
            if(gridData.Name == "")
            {
                gridData.Name = name;
            }
            gridData.Anchor = transform;
            grid = new NaviGridHandler(gridData);
            //grid = new Grid<GridNode>(gridData);
        }

        private void OnDrawGizmos()
        {
            if(grid != null && drawGizmo)
            {
                for(int i = 0; i < gridData.WorldPositions.Length; i++)
                {

                    //float col = (float)grid.GetNodeID(pos) / (float)gridData.WorldPositions.Length;
                    //Gizmos.color = new Color(col, col, col);
                    if (gridData.Status[i] == NavGridData.FieldStatus.Blocked)
                    {
                        Gizmos.color = Color.red;
                    }
                    else
                    {
                        Gizmos.color = Color.gray;
                    }

                    Gizmos.DrawWireCube(gridData.WorldPositions[i], gridData.Size * Vector3.one * 0.9f);
                }
            }
        }
    }
}