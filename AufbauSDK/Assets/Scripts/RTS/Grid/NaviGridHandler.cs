using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Grid.Data;

namespace Rts.Grid
{
    public class NaviGridHandler : RawGridHandler<NavGridData>
    {
        protected struct TempGridData
        {
            public int[] cameFrom;
            public float[] moveCost;
            public float[] heuristicCost;

            public List<int> openList;
            //public List<int> closedList;

            public bool[] closedFields;
        }
        public NaviGridHandler(NavGridData _data) : base(_data)
        {
            
        }

        protected override void CreateGrid()
        {
            data.Status = new NavGridData.FieldStatus[data.Width * data.Height];
            base.CreateGrid();
        }

        protected override void CreateNode(int _id, int _x, int _y)
        {
            Vector3 tempWorldPos = data.Anchor.TransformPoint(new Vector3(
                _x * data.Size + data.Size * 0.5f,
                0,
                _y * data.Size + data.Size * 0.5f
                ));

            RaycastHit hit;
            if(Physics.Raycast(
                tempWorldPos + (Vector3.up * data.MaxWorldHeight),
                Vector3.down,
                out hit,
                data.MaxWorldHeight - data.MinWorldHeight + 1,
                data.GroundLayer
                ))
            {
                tempWorldPos.y = hit.point.y;
                data.WorldPositions[_id] = tempWorldPos;
            }

            if (Physics.CheckSphere(data.WorldPositions[_id], (data.Size / 2) * 0.9f, data.BlockedLayer))
                data.Status[_id] = NavGridData.FieldStatus.Blocked;
            if (Physics.CheckSphere(data.WorldPositions[_id], (data.Size / 2) * 0.9f, data.BuildableLayer))
                data.Status[_id] = NavGridData.FieldStatus.Buildable;
        }

        public List<int> GetRoute(int _startID, int _endID)
        {
            TempGridData temp = new TempGridData();
            temp.openList = new List<int>();
            //temp.closedList = new List<int>();

            temp.cameFrom = new int[data.Width * data.Height];
            temp.moveCost = new float[data.Width * data.Height];
            temp.heuristicCost = new float[data.Width * data.Height];
            temp.closedFields = new bool[data.Width * data.Height];

            for (int i = 0; i < temp.cameFrom.Length; i++)
            {
                temp.cameFrom[i] = -1;
                temp.moveCost[i] = float.MaxValue;
                temp.heuristicCost[i] = float.MaxValue;
                temp.closedFields[i] = false;
            }

            int currentID = _startID;
            temp.openList.Add(currentID);

            temp.moveCost[currentID] = 0;
            temp.heuristicCost[currentID] = Vector3.Distance(GetWorldPosition(currentID), GetWorldPosition(_endID));

            while (temp.openList.Count > 0)
            {
                currentID = temp.openList[0];
                foreach (int node in temp.openList)
                {
                    if ((temp.moveCost[node] + temp.heuristicCost[node]) < (temp.moveCost[currentID] + temp.heuristicCost[currentID]))
                    {
                        currentID = node;
                    }
                }


                if (currentID == _endID)
                    return GetPath(temp.cameFrom, _startID, _endID);

                temp.openList.Remove(currentID);

                Vector2Int gridPos = GetGridPosition(currentID);
                //Top Left
                //tempNeighbour = GetNodeID(gridPos.x - 1, gridPos.y + 1);
                AddNeighbour(gridPos + new Vector2Int(-1,+1), currentID, _endID, ref temp);

                //Top Mid
                //tempNeighbour = GetNodeID(gridPos.x, gridPos.y + 1);
                AddNeighbour(gridPos + new Vector2Int(0, +1), currentID, _endID, ref temp);
               
                //Top Right
                //tempNeighbour = GetNodeID(gridPos.x + 1, gridPos.y + 1);
                AddNeighbour(gridPos + new Vector2Int(+1, +1), currentID, _endID, ref temp);
               
                //Center Left
                //tempNeighbour = GetNodeID(gridPos.x - 1, gridPos.y);
                AddNeighbour(gridPos + new Vector2Int(-1, 0), currentID, _endID, ref temp);
                
                //Center Right
                //tempNeighbour = GetNodeID(gridPos.x + 1, gridPos.y);
                AddNeighbour(gridPos + new Vector2Int(+1, 0), currentID, _endID, ref temp);
                
                //Bottom Left
                //tempNeighbour = GetNodeID(gridPos.x - 1, gridPos.y - 1);
                AddNeighbour(gridPos + new Vector2Int(-1, -1), currentID, _endID, ref temp);
                
                //Bottom Mid
                //tempNeighbour = GetNodeID(gridPos.x, gridPos.y - 1);
                AddNeighbour(gridPos + new Vector2Int(0, -1), currentID, _endID, ref temp);
                
                //Botton Right
                //tempNeighbour = GetNodeID(gridPos.x + 1, gridPos.y - 1);
                AddNeighbour(gridPos + new Vector2Int(+1, -1), currentID, _endID, ref temp);

                //temp.closedList.Add(currentID);
                temp.closedFields[currentID] = true;
            }
            return null;
        }

        protected void AddNeighbour(Vector2Int _gridPos, int _current, int _end, ref TempGridData _data)
        {
            int neighbour = -1;
            if((_gridPos.x >= 0 && _gridPos.x < data.Width) && (_gridPos.y >= 0 && _gridPos.y < data.Height))
            {
                neighbour = GetNodeID(_gridPos.x, _gridPos.y);

                //if (!_data.closedList.Contains(neighbour))
                if (!_data.closedFields[neighbour] && !(data.Status[neighbour] == NavGridData.FieldStatus.Blocked))
                {
                    float potentiallyCost = _data.moveCost[_current] + Vector3.Distance(GetWorldPosition(_current), GetWorldPosition(neighbour));

                    if (potentiallyCost < _data.moveCost[neighbour])
                    {
                        _data.cameFrom[neighbour] = _current;
                        _data.moveCost[neighbour] = potentiallyCost;
                        _data.heuristicCost[neighbour] = Vector3.Distance(GetWorldPosition(neighbour), GetWorldPosition(_end));

                        if (!_data.openList.Contains(neighbour))
                            _data.openList.Add(neighbour);
                    }
                }
            }
        }

        private List<int> GetPath(int[] _cameFrom, int _start, int _end)
        {
            List<int> path = new List<int>();
            int currentID = _end;
            while (currentID != _start)
            {
                path.Add(currentID);
                currentID = _cameFrom[currentID];
            }
            path.Reverse();
            return path;
        }
    }
}