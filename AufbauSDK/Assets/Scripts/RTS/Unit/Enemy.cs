using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Unit.Data;
using Rts.Unit;
using UnityEngine.Events;

public class Enemy : Unit<EnemyData>
{
    private NaviAgent agent;
    public NaviAgent Agent
    {
        get { return agent; }
    }

    [SerializeField]
    private int currentHitpoint = 1;
    public int Hitpoint
    {
        get
        {
            return currentHitpoint;
        }
        set
        {
            currentHitpoint = value;
            if(currentHitpoint > Data.Hitpoint)
                currentHitpoint = Data.Hitpoint;
            if (currentHitpoint <= 0)
                Destroy(this.gameObject);
        }
    }

    public UnityEvent<Enemy> OnDestroyAction;

    private void Awake()
    {
        Debug.Log("Ich Spawne");
    }

    private void Start()
    {
        
    }

    public void Init(Rts.Grid.NaviGridHandler _naviGrid)
    {
        agent = GetComponent<NaviAgent>();
        agent.SetGrid(_naviGrid);
        agent.MovementSpeed = Data.Speed;
    }

    public void MoveTo(Vector3 _pos)
    {
        agent.MoveTo(_pos);
    }

    private void OnDestroy()
    {
        OnDestroyAction.Invoke(this);
    }
}
