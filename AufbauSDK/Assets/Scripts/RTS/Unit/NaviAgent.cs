using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Grid;

namespace Rts.Unit
{
    public class NaviAgent : MonoBehaviour
    {
        private float targetDistance = 0.1f;
        public float MovementSpeed = 3.0f;
        private int currentIndex = 0;
        private Vector3 nextPosition;
        private List<int> currentPath;

        private NaviGridHandler currentGrid;

        private void Update()
        {
            Move();
        }

        public void SetGrid(NaviGridHandler _grid)
        {
            currentGrid = _grid;
        }

        public void Move()
        {
            if (currentPath == null)
            {
                return;
            }

            if (Vector3.Distance(nextPosition, transform.position) < targetDistance)
            {
                if(currentIndex < currentPath.Count)
                {
                    nextPosition = currentGrid.GetWorldPosition(currentPath[currentIndex]);
                    currentIndex++;
                }
                else
                {
                    currentIndex = 0;
                    currentPath = null;
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, nextPosition, MovementSpeed * Time.deltaTime);
            }
        }

        public void MoveTo(Vector3 _targetPosition)
        {
            if (currentGrid == null)
            {
                return;
            }
            currentPath = currentGrid.GetRoute(currentGrid.GetNodeID(transform.position), currentGrid.GetNodeID(_targetPosition));
            nextPosition = currentGrid.GetWorldPosition(currentGrid.GetNodeID(transform.position));
            currentIndex = 0;
        }

        public int RestPath()
        {
            if(currentPath != null)
            {
                return currentPath.Count - currentIndex;
            }
            else
            {
                return 0;
            }
        }

        //private void OnDrawGizmos()
        //{
        //    if(currentPath != null)
        //    {
        //        List<Vector3> tempPos = new List<Vector3>();
        //        foreach(int p in currentPath)
        //        {
        //            tempPos.Add(currentGrid.GetWorldPosition(p));
        //        }
        //        Gizmos.DrawLineList(tempPos.ToArray());
        //    }
        //}
    }
}