using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rts.Unit.Data
{
    [CreateAssetMenu(fileName ="EnemyData", menuName ="RTS/Unit/Enemy", order =0)]
    public class EnemyData : UnitData
    {
        public int Damage;
        public int Lifecount;
    }
}