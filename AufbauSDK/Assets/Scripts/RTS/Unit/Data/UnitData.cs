using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rts.Unit.Data
{
    public class UnitData : ScriptableObject
    {
        public string Name;
        public int Hitpoint;
        public int Speed;
    }
}