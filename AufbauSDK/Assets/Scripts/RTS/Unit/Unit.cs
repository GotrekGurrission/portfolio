using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rts.Unit.Data;

public class Unit<data> : MonoBehaviour where data : UnitData
{
    public data Data;
}
