import os
os.environ["OPENCV_IO_ENABLE_OPENEXR"]="1"
import numpy as np
import cv2 as cv
import argparse as ap

# This script takes a texture image and converts it to the desired format.
# Supported input formats: .jpg, .png, .bmp, .exr, .hdr
# Supported output formats: .jpg, .png, .bmp, .exr, .hdr

tm_burnout = 1.0

# main function
def main():    
    # parse arguments
    parser = ap.ArgumentParser()
    parser.add_argument("input", help="input texture image file")
    parser.add_argument("output", help="output texture image file")
    parser.add_argument("-il", "--inputlinear", help="Specifies that the intput is linear", action="store_true", default=False)
    parser.add_argument("-ol", "--outputlinear", help="Specifies that the output should be linear", action="store_true", default=False)
    parser.add_argument("-ig", "--inputgamma", help="Specifies input gamma value", type=float, default=2.2)
    parser.add_argument("-og", "--outputgamma", help="Specifies output gamma value", type=float, default=2.2)
    parser.add_argument("-tm", "--tonemap", help="Specifies that the input image should be tone mapped", action="store_true", default=False)
    args = parser.parse_args()

    # throw error if image format is not supported
    if not (args.input.endswith(".jpg") or args.input.endswith(".png") or args.input.endswith(".bmp") or args.input.endswith(".exr") or args.input.endswith(".hdr")):
        print("ERROR: Input image format not supported.")
        return

    # read input image
    img = cv.imread(args.input, cv.IMREAD_UNCHANGED)
    if img is None:
        print("ERROR: Could not read input image.")
        return

    # if input image is not float32 yet, convert it to float32
    if img.dtype == np.uint8:
        img = img.astype(np.float32) / 255.0

    # convert input image to linear if it is not already 
    if not args.inputlinear:
        # inverse gamma correction
        img = np.power(img, args.inputgamma) 
    
    # if input image is .exr or .hdr tone map, and output .jpg, .png or .bmp, tone map image
    if (args.tonemap):
        # get image luminance
        l_old = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        # tone map image (Reinhard et al. 2002)
        # numerator = l_old * (1.0 + (l_old / (tm_burnout * tm_burnout)))
        # l_new = numerator / (1.0 + l_old)
        l_new = l_old / (1.0 + l_old)
        # apply tone mapping to image
        img = img * (l_new / l_old)[..., np.newaxis]

    # convert image to output gamma
    if not args.outputlinear:
        # gamma correction
        img = np.power(img, 1.0 / args.outputgamma)

    # if .jpg, .png or .bmp, convert image to uint8
    if args.output.endswith(".jpg") or args.output.endswith(".png") or args.output.endswith(".bmp"):
        img = np.clip(img, 0.0, 1.0)
        img = (img * 255.0).astype(np.uint8)        
    
    # save image
    cv.imwrite(args.output, img)

if __name__ == "__main__":
    main()